package com.htcindia.matchrx_30;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.Result;

import java.io.IOException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanningActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    SurfaceView mSurfaceView;
    TextView tvScannedText;
    CameraSource cameraSource;

    SparseArray<Barcode> codes;

    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                       // Set the scanner view as the content view

//        setContentView(R.layout.activity_scanning);

//        mSurfaceView = findViewById(R.id.mSurfaceView);
//        tvScannedText = findViewById(R.id.tvScannedText);
//
//        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this).build();
//        cameraSource = new CameraSource.Builder(this, barcodeDetector)
//                .setAutoFocusEnabled(true)
//                .setRequestedPreviewSize(1600, 1080)
//                .build();
//
//
//        mSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
//            @Override
//            public void surfaceCreated(SurfaceHolder holder) {
//                try {
//                    if (ActivityCompat.checkSelfPermission(ScanningActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        return;
//                    }
//                    cameraSource.start(holder);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//            }
//
//            @Override
//            public void surfaceDestroyed(SurfaceHolder holder) {
//                cameraSource.stop();
//            }
//        });
//
//        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
//            @Override
//            public void release() {
//
//            }
//
//            @Override
//            public void receiveDetections(Detector.Detections<Barcode> detections) {
//                codes = detections.getDetectedItems();
//                if (codes.size() > 0) {
//                    Log.d("QR CODE 1 : ", codes.valueAt(0).displayValue);
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Log.d("QR CODE 2 : ", codes.valueAt(0).displayValue);
//                            tvScannedText.setText(codes.valueAt(0).displayValue);
//                        }
//                    });
//
////                    Intent intent = new Intent();
////                    intent.putExtra("CODE", codes.valueAt(0).displayValue);
////                    setResult(RESULT_OK, intent);
////                    finish();
//                }
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    protected void onStop() {
        super.onStop();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {

        Log.v("CODE : ", result.getText()); // Prints scan results
        Log.v("CODE FORMAT : ", result.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        Intent intent = new Intent();
        intent.putExtra("NDC_BARCODE_NO", result.getText());
        setResult(RESULT_OK, intent);
        finish();

    }
}
