package com.htcindia.matchrx_30.webservice;

/**
 * Created by sathishk on 11/21/2017.
 */

public interface OnWebResponse {
    public void onSuccess(Object response, int type);
    public void onFailure(String reason, int type);
}
