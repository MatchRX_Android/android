package com.htcindia.matchrx_30.webservice;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.htcindia.matchrx_30.model.request.ForgotPasswordReq;
import com.htcindia.matchrx_30.model.request.LoginReq;
import com.htcindia.matchrx_30.model.response.ForgotPasswordResp;
import com.htcindia.matchrx_30.model.response.LoginResp;
import com.htcindia.matchrx_30.model.response.PharmacyInfoResp;
import com.htcindia.matchrx_30.utils.MatchRx;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sathishk on 11/21/2017.
 */

public class WebRequestHelper {

    private Context mContext;
    private String BASE_URL;
    private String path = "";
    private Response.ErrorListener errorListener;
    private Response.Listener successListener;

    public WebRequestHelper(Context mContext, Response.ErrorListener errorListener, Response.Listener successListener) {
        this.mContext = mContext;
        this.errorListener = errorListener;
        this.successListener = successListener;
        initBaseURL();
    }

    private void initBaseURL() {
//        BASE_URL = "https://matchrx.htcindia.com/mrx/api/v3/";
        BASE_URL = "https://matchrxqa.htcindia.com/mrx/api/v3/";
    }

    private String getURL() {
        return BASE_URL + path;
    }

    public Request LoginRequest(LoginReq loginReq) {
        path = "users/login";

        Log.d("LOGIN URL : ", getURL());
        Log.d("LOGIN WEB REQUEST : ", new Gson().toJson(loginReq));

        JsonRequest<LoginResp> jsonRequest = new JsonRequest<LoginResp>(Request.Method.POST, getURL(), new Gson().toJson(loginReq), successListener, errorListener) {
            @Override
            protected Response<LoginResp> parseNetworkResponse(NetworkResponse networkResponse) {
                String parsed = "";
                try {
                    parsed = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                } catch (UnsupportedEncodingException var4) {
                    parsed = new String(networkResponse.data);
                }

                Log.d("LOGIN WEB RESPONSE : ", parsed);

                LoginResp loginResp = null;
                try {
                    loginResp = new Gson().fromJson(parsed, LoginResp.class);
                } catch (Exception e) {
                    return Response.error(new VolleyError("Invalid response format!"));
                }
                return Response.success(loginResp, HttpHeaderParser.parseCacheHeaders(networkResponse));
            }
        };
        return jsonRequest;
    }

    public Request ForgotPasswordRequest(ForgotPasswordReq forgotPasswordReq) {
        path = "users/forgotpassword";

        Log.d("FORGOT PSWD URL : ", getURL());
        Log.d("FORGOT PSWD REQUEST : ", new Gson().toJson(forgotPasswordReq));

        JsonRequest<ForgotPasswordResp> jsonRequest = new JsonRequest<ForgotPasswordResp>(Request.Method.POST, getURL(), new Gson().toJson(forgotPasswordReq), successListener, errorListener) {
            @Override
            protected Response<ForgotPasswordResp> parseNetworkResponse(NetworkResponse networkResponse) {
                String parsed = "";
                try {
                    parsed = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                } catch (UnsupportedEncodingException var4) {
                    parsed = new String(networkResponse.data);
                }

                Log.d("FORGOT PSWD RESPONSE : ", parsed);

                ForgotPasswordResp forgotPasswordResp = null;
                try {
                    forgotPasswordResp = new Gson().fromJson(parsed, ForgotPasswordResp.class);
                } catch (Exception e) {
                    return Response.error(new VolleyError("Invalid response format!"));
                }
                return Response.success(forgotPasswordResp, HttpHeaderParser.parseCacheHeaders(networkResponse));
            }
        };
        return jsonRequest;
    }

    public Request PharmacyInfo(long pharmacyId) {
        path = "users/" + pharmacyId;

        JsonRequest<PharmacyInfoResp> jsonRequest = new JsonRequest<PharmacyInfoResp>(Request.Method.GET, getURL(), new Gson().toJson(new String()), successListener, errorListener) {
            @Override
            protected Response<PharmacyInfoResp> parseNetworkResponse(NetworkResponse networkResponse) {
                String parsed = "";
                try {
                    parsed = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                } catch (UnsupportedEncodingException var4) {
                    parsed = new String(networkResponse.data);
                }

                Log.d("PHARMACY RESPONSE : ", parsed);

                PharmacyInfoResp pharmacyInfoResp = null;
                try {
                    pharmacyInfoResp = new Gson().fromJson(parsed, PharmacyInfoResp.class);
                } catch (Exception e) {
                    return Response.error(new VolleyError("Invalid response format!"));
                }
                return Response.success(pharmacyInfoResp, HttpHeaderParser.parseCacheHeaders(networkResponse));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", MatchRx.TOKEN.getToken_type() + " " + MatchRx.TOKEN.getAccess_token());
                return params;
            }
        };
        return jsonRequest;
    }
}
