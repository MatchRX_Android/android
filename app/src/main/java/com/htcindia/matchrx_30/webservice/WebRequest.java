package com.htcindia.matchrx_30.webservice;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.htcindia.matchrx_30.model.response.ErrorResponse;
import com.htcindia.matchrx_30.model.request.ForgotPasswordReq;
import com.htcindia.matchrx_30.model.request.LoginReq;

/**
 * Created by sathishk on 11/21/2017.
 */

public class WebRequest implements Response.Listener, Response.ErrorListener {

    private Context mContext;
    private WebRequestHelper webRequest;
    private OnWebResponse mCallback = null;
    private int type;
    private Request request;

    public WebRequest(Context mContext, OnWebResponse mCallback, int type) {
        this.mContext = mContext;
        this.webRequest = new WebRequestHelper(mContext, this, this);
        this.mCallback = mCallback;
        this.type = type;
    }

    public void LoginRequest(String deaNo, String username, String password) {
        LoginReq loginReq = new LoginReq(deaNo, username, password);
        request = webRequest.LoginRequest(loginReq);
        execute();
    }

    public void ForgotPasswordRequest(String deaNo, String username) {
        ForgotPasswordReq forgotPasswordReq = new ForgotPasswordReq(username, deaNo);
        request = webRequest.ForgotPasswordRequest(forgotPasswordReq);
        execute();
    }

    public void PharmacyInfo(long pharmacyId) {
        request = webRequest.PharmacyInfo(pharmacyId);
        execute();
    }

    public void execute() {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            requestQueue.add(request);
        } catch (Exception e) {
            mCallback.onFailure("Something went wrong! Try after sometime.", -1);
        }
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {

        if (volleyError instanceof NetworkError) {
            mCallback.onFailure("Network error", type);
        } else if (volleyError instanceof ServerError) {
            if (volleyError.networkResponse.statusCode == 422) {
                String errorResp = new String(volleyError.networkResponse.data);
                ErrorResponse errorResponse = null;
                try {
                    errorResponse = new Gson().fromJson(errorResp, ErrorResponse.class);
                    mCallback.onFailure(errorResponse.getData().getMessage(), type);
                } catch (Exception e) {
                    mCallback.onFailure("Server exception", type);
                }
            } else {
                mCallback.onFailure("Server exception", type);
            }
        } else if (volleyError instanceof AuthFailureError) {
            mCallback.onFailure("Authentication error", type);
        } else if (volleyError instanceof ParseError) {
            mCallback.onFailure("Parse exception", type);
        } else if (volleyError instanceof NoConnectionError) {
            mCallback.onFailure("Please check internet connection", type);
        } else if (volleyError instanceof TimeoutError) {
            mCallback.onFailure("Timeout exception", type);
        } else {
            mCallback.onFailure(volleyError.getMessage(), type);
        }
    }

    @Override
    public void onResponse(Object response) {
        mCallback.onSuccess(response, type);
    }

    public static class Type {
        public static final int LOGIN = 0;
        public static final int FORGOT_PASSWORD = 1;
        public static final int PHARMACY_INFO = 2;
    }
}
