package com.htcindia.matchrx_30;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.htcindia.matchrx_30.adapter.MonthAdapter;
import com.htcindia.matchrx_30.adapter.YearAdapter;
import com.htcindia.matchrx_30.utils.Constants;

import java.util.Calendar;

public class DummyActivity extends BaseActivity {

    TextView tvProgress;
    SeekBar seekBar;
    TextView tvScannedCode;
    Intent scannerIntent;
    Button btnMonthPicker;
    float screenWidth;
    float screenHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_dummy);

        seekBar = findViewById(R.id.seekBar);
        tvProgress = findViewById(R.id.tvProgress);
        tvScannedCode = findViewById(R.id.tvScannedCode);
        scannerIntent = new Intent(DummyActivity.this, ScanningActivity.class);

        Button btnMonthPicker = findViewById(R.id.btnMonthPicker);
        getScreenSize();

        Button btnScan = findViewById(R.id.btnScan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                Constants.PERMISSION_REQUEST_CAMERA);
                    } else {
                        startActivityForResult(scannerIntent, Constants.SCANNING_REQ_CODE);
                    }
                } else {
                    startActivityForResult(scannerIntent, Constants.SCANNING_REQ_CODE);
                }

            }
        });


        btnMonthPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMonthYearPicker();
            }
        });


        int val = (10 * (seekBar.getWidth() - 4 * seekBar.getThumbOffset())) / seekBar.getMax();
        tvProgress.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int val = (progress * (seekBar.getWidth() - 4 * seekBar.getThumbOffset())) / seekBar.getMax();
                tvProgress.setText("" + (progress + 10) + "%");
                tvProgress.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
//                if (seekBar.getProgress() < 10) {
//                    seekBar.setProgress(10);
//                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    View view;
    MonthAdapter monthAdapter;

    private View getCalendarDialogView() {

        view = View.inflate(this, R.layout.sell_calendar_dialog_layout, null);

        final ViewFlipper vfCalendar = view.findViewById(R.id.vfCalendar);

        View tvBack = view.findViewById(R.id.ivBack);
        final TextView tvYear = view.findViewById(R.id.tvYear);

        GridView gvYear = view.findViewById(R.id.gvYear);
        gvYear.setNumColumns(3);

        GridView gvMonth = view.findViewById(R.id.gvMonth);
        gvMonth.setNumColumns(3);

        YearAdapter yearAdapter = new YearAdapter(this);
        gvYear.setAdapter(yearAdapter);

        monthAdapter = new MonthAdapter(this);
        gvMonth.setAdapter(monthAdapter);

        gvYear.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateMonthPicker(((TextView) view.findViewById(R.id.calendarTxt)));
                vfCalendar.setDisplayedChild(1);
            }
        });

        gvMonth.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= Calendar.getInstance().get(Calendar.MONTH)) {
                    monthYearPickerDialog.dismiss();
                }
            }
        });

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vfCalendar.setDisplayedChild(0);
            }
        });

        return view;

    }

    public void updateMonthPicker(TextView yearText) {
        String selectedYear = yearText.getText().toString();
        TextView tvYear = view.findViewById(R.id.tvYear);
        tvYear.setText(selectedYear);

        Calendar calendar = Calendar.getInstance();
        if (calendar.get(Calendar.YEAR) == Integer.parseInt(selectedYear)) {
            monthAdapter.isCurrentYear(true);
            monthAdapter.notifyDataSetChanged();
        } else {
            monthAdapter.isCurrentYear(false);
            monthAdapter.notifyDataSetChanged();
        }
    }

    Dialog monthYearPickerDialog;

    private void showMonthYearPicker() {
        monthYearPickerDialog = new Dialog(this);
        monthYearPickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        monthYearPickerDialog.setContentView(getCalendarDialogView());
        monthYearPickerDialog.getWindow().setLayout((int) (screenWidth * 0.95), (int) (screenHeight * 0.46));
        monthYearPickerDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.SCANNING_REQ_CODE && resultCode == RESULT_OK) {
            if (data.hasExtra("CODE")) {
                tvScannedCode.setText(data.getStringExtra("CODE"));
            }
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case Constants.PERMISSION_REQUEST_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(new Intent(DummyActivity.this, ScanningActivity.class), Constants.SCANNING_REQ_CODE);
                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            1003);
                }
                break;
        }
    }

    private void getScreenSize() {
        WindowManager localWindowManager = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);
        localWindowManager.getDefaultDisplay().getMetrics(new DisplayMetrics());
        Point localPoint = getSize(localWindowManager);
        screenWidth = localPoint.x;
        screenHeight = localPoint.y;
    }

    @SuppressLint("NewApi")
    private Point getSize(WindowManager paramWindowManager) {
        Point localPoint = new Point();
        if (Build.VERSION.SDK_INT >= 13) {
            paramWindowManager.getDefaultDisplay().getSize(localPoint);
            return localPoint;
        }
        Display localDisplay = paramWindowManager.getDefaultDisplay();
        localPoint.x = localDisplay.getWidth();
        localPoint.y = localDisplay.getHeight();
        return localPoint;
    }
}
