package com.htcindia.matchrx_30.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.htcindia.matchrx_30.R;

/**
 * Created by sathishk on 11/27/2017.
 */

public class CTextView extends android.support.v7.widget.AppCompatTextView {

    private int fontFamily = 1;

    public CTextView(Context context) {
        super(context);
        init(null);
    }

    public CTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        if (!isInEditMode()) {
            if (attrs != null) {
                TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.FontStyle, 0, 0);
                try {
                    fontFamily = ta.getInteger(R.styleable.FontStyle_CFontFamily, 1);
                } finally {
                    ta.recycle();
                }
            } else {

            }

            Typeface typeface = getTypeface();
            switch (fontFamily) {
                case 1:
                    typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/RobotoCondensed-Regular.ttf");
                    break;
                case 2:
                    typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                    break;
                case 3:
                    typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
                    break;
                case 4:
                    typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Medium.ttf");
                    break;
                case 5:
                    typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Bold.ttf");
                    break;
                default:
                    break;
            }
            setTypeface(typeface);
        }
    }

    public void setFont(int fontId) {
        fontFamily = fontId;
    }
}
