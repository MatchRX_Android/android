package com.htcindia.matchrx_30.fragment.myaccount;

import java.text.SimpleDateFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.model.Pharmacy;

import java.text.ParseException;


public class LicenseFragment extends Fragment {

    private Pharmacy.PharmacyLicenseInfo pharmacyLicenseInfo;

    public LicenseFragment() {

    }

    public void setPharmacyLicenseInfo(Pharmacy.PharmacyLicenseInfo pharmacyLicenseInfo) {
        this.pharmacyLicenseInfo = pharmacyLicenseInfo;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_account_license, container, false);
        if (pharmacyLicenseInfo != null) {
            ((TextView) view.findViewById(R.id.tvNcpdpNo)).setText(pharmacyLicenseInfo.getNcpdp_number());
            ((TextView) view.findViewById(R.id.tvDeaLicenseNo)).setText(pharmacyLicenseInfo.getDea());

            String date = "";
            try {
                date = new SimpleDateFormat("MM/dd/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(pharmacyLicenseInfo.getDea_exp_date()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ((TextView) view.findViewById(R.id.tvDeaExpDate)).setText(date);

            ((TextView) view.findViewById(R.id.tvNpiNo)).setText(pharmacyLicenseInfo.getNpi_number().toString());
            ((TextView) view.findViewById(R.id.tvStateLicenseNo)).setText(pharmacyLicenseInfo.getState_license_number());

            try {
                date = new SimpleDateFormat("MM/dd/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(pharmacyLicenseInfo.getState_license_exp_date()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ((TextView) view.findViewById(R.id.tvStateLicenseExp)).setText(date);
        }

        return view;
    }
}
