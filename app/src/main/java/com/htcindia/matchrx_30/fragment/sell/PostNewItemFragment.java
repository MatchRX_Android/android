package com.htcindia.matchrx_30.fragment.sell;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.htcindia.matchrx_30.LandingActivity;
import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.ScanningActivity;
import com.htcindia.matchrx_30.listeners.ScannerListener;
import com.htcindia.matchrx_30.listeners.SnackBarListener;
import com.htcindia.matchrx_30.utils.Constants;
import com.htcindia.matchrx_30.webservice.OnWebResponse;
import com.htcindia.matchrx_30.webservice.WebRequest;

import static android.app.Activity.RESULT_OK;


public class PostNewItemFragment extends Fragment implements View.OnClickListener, OnWebResponse {


    private ScannerListener scannerListener;
    private SnackBarListener snackBarListener;

    FrameLayout sellContainer;

    EnterNDCFragment enterNDCFragment;

    public PostNewItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void searchNDCDrug(String ndcNo) {
        enterNDCFragment.searchNDCDrug(ndcNo);

        //Call NDC Verification Service

    }

    public void setScannerListener(ScannerListener scannerListener) {
        this.scannerListener = scannerListener;
    }

    public void setSnackBarListener(SnackBarListener snackBarListener) {
        this.snackBarListener = snackBarListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_new_item, container, false);
        init(view);

        getChildFragmentManager().beginTransaction().replace(R.id.sellContainer, enterNDCFragment).commit();

        return view;
    }

    public void init(View view) {

        sellContainer = view.findViewById(R.id.sellContainer);

        enterNDCFragment = new EnterNDCFragment();
        enterNDCFragment.setScannerListener(scannerListener);
        enterNDCFragment.setSnackBarListener(snackBarListener);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        scannerListener = null;
        snackBarListener=null;
    }

    @Override
    public void onSuccess(Object response, int type) {

    }

    @Override
    public void onFailure(String reason, int type) {

    }
}
