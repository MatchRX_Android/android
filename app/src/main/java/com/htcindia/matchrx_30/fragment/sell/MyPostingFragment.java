package com.htcindia.matchrx_30.fragment.sell;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.htcindia.matchrx_30.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPostingFragment extends Fragment {


    public MyPostingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_posting, container, false);
    }

}
