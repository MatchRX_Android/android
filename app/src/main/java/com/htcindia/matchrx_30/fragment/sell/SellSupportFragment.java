package com.htcindia.matchrx_30.fragment.sell;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.fragment.BaseFragment;
import com.htcindia.matchrx_30.model.response.NDCVerificationResp;
import com.htcindia.matchrx_30.utils.NDC;
import com.squareup.picasso.Picasso;

/**
 * Created by sathishk on 12/15/2017.
 */

public class SellSupportFragment extends BaseFragment {

    //Validate NDC Number
    protected boolean validateNDC(String NDCNo) {
        int digitsCounter = 0;
        int hyphenCounter = 0;
        if (NDCNo.length() < 10) {
            return false;
        }
        for (int i = 0; i < NDCNo.length(); i++) {
            if (NDCNo.charAt(i) == '-') {
                if (i == 0 || i == (NDCNo.length() - 1)) {
                    return false;
                }
                hyphenCounter++;
                if (hyphenCounter > 2) {
                    return false;
                } else if (NDCNo.charAt(i - 1) == '-') {
                    return false;
                }
            }
            if (Character.isDigit(NDCNo.charAt(i))) {
                digitsCounter++;
                if (digitsCounter > 11) {
                    return false;
                }
            }
        }
        if (digitsCounter < 10) {
            return false;
        }
        return true;
    }

    protected NDCVerificationResp.NDCVerificationData loadNDCVerificationData(String ndcNo) {

        NDCVerificationResp ndcVerificationResp = new NDCVerificationResp();

        if (ndcNo.equals(NDC.PHENADOZ_NDC)) {
            try {
                ndcVerificationResp = new Gson().fromJson(NDC.PHENADOZ, NDCVerificationResp.class);
            } catch (Exception e) {
                Log.e("NDC PARSING ERROR :", e.getMessage());
            }
        } else if (ndcNo.equals(NDC.DYRENIUM_NDC)) {
            try {
                ndcVerificationResp = new Gson().fromJson(NDC.DYRENIUM, NDCVerificationResp.class);
            } catch (Exception e) {
                Log.e("NDC PARSING ERROR :", e.getMessage());
            }
        } else if (ndcNo.equals(NDC.CARBAMAZEPINE_ER_NDC)) {
            try {
                ndcVerificationResp = new Gson().fromJson(NDC.CARBAMAZEPINE_ER, NDCVerificationResp.class);
            } catch (Exception e) {
                Log.e("NDC PARSING ERROR :", e.getMessage());
            }
        } else if (ndcNo.equals(NDC.CELLCEPT_NDC)) {
            try {
                ndcVerificationResp = new Gson().fromJson(NDC.CELLCEPT, NDCVerificationResp.class);
            } catch (Exception e) {
                Log.e("NDC PARSING ERROR :", e.getMessage());
            }
        }
        return ndcVerificationResp.getData();
    }

    //Load Medi Span Images
    protected void loadMediSpanDrugPic(String ndcNo, ImageView ivMediSpanDrugPic) {

        if (ndcNo.equals("00591-2985-39")) {
            //phenadoz
            Picasso.with(getActivity()).load(R.drawable.phenadoz).fit().into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load(R.drawable.phenadoz).into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load("http://mapp2.htcindia.com:8080/matchRx/PHENADOZ.jpg").fit().into(ivMediSpanDrugPic);
        } else if (ndcNo.equals("65197-0002-01")) {
            //dyrenium
            Picasso.with(getActivity()).load(R.drawable.dyrenium).fit().into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load(R.drawable.dyrenium).into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load("http://mapp2.htcindia.com:8080/matchRx/DYRENIUM.jpg").fit().into(ivMediSpanDrugPic);
        } else if (ndcNo.equals("60505-2806-07")) {
            //carbamazepine_er
            Picasso.with(getActivity()).load(R.drawable.carbamazepine_er).fit().into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load(R.drawable.carbamazepine_er).into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load("http://mapp2.htcindia.com:8080/matchRx/CARBAMAZEPINE%20ER.jpg").fit().into(ivMediSpanDrugPic);
        } else if (ndcNo.equals("00004-0259-01")) {
            //cellcept
            Picasso.with(getActivity()).load(R.drawable.cellcept).fit().into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load(R.drawable.cellcept).into(ivMediSpanDrugPic);
//            Picasso.with(getActivity()).load("http://mapp2.htcindia.com:8080/matchRx/CELLCEPT.jpg").fit().into(ivMediSpanDrugPic);
        }
    }

    protected String pickStorageType(Character storage) {
        switch (storage) {

            case 'R':
                return "Refrigerated";

            case 'F':
                return "Frozen";

            case 'N':
                return "Normal";
        }
        return "NA";
    }

    //Load Spinner Values
    protected void populatePackAvailable(int packAvailable, Spinner spPackageAvailable) {

        String[] packageArray = new String[packAvailable];
        for (int i = 0; i < packAvailable; i++) {
            packageArray[i] = String.valueOf(i + 1);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, packageArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spPackageAvailable.setAdapter(adapter);
        spPackageAvailable.setSelection(0);
    }
}
