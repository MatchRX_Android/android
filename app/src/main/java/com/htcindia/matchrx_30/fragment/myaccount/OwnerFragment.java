package com.htcindia.matchrx_30.fragment.myaccount;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.model.Pharmacy;
import com.htcindia.matchrx_30.model.PharmacyInfo;


public class OwnerFragment extends Fragment {

    private Pharmacy.PharmacyOwnerInformation pharmacyOwnerInfo;

    public OwnerFragment() {

    }

    public void setPharmacyOwnerInfo(Pharmacy.PharmacyOwnerInformation pharmacyOwnerInfo) {
        this.pharmacyOwnerInfo = pharmacyOwnerInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_account__owner, null);
        if (pharmacyOwnerInfo != null) {
            ((TextView) view.findViewById(R.id.tvFirstName)).setText(pharmacyOwnerInfo.getOwner_first_name());
            ((TextView) view.findViewById(R.id.tvLastName)).setText(pharmacyOwnerInfo.getOwner_last_name());
            ((TextView) view.findViewById(R.id.tvMobile)).setText(pharmacyOwnerInfo.getOwner_mobile());
            ((TextView) view.findViewById(R.id.tvEmailAddress)).setText(pharmacyOwnerInfo.getOwner_email());
        }

        return view;
    }
}
