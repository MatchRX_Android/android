package com.htcindia.matchrx_30.fragment.myaccount;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.model.Pharmacy;
import com.htcindia.matchrx_30.model.PharmacyInfo;


public class PharmacyFragment extends Fragment {

    private Pharmacy.PharmacyInformation pharmacyInfo;

    public PharmacyFragment() {
    }

    public void setPharmacyInfo(Pharmacy.PharmacyInformation pharmacyInfo) {
        this.pharmacyInfo = pharmacyInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_account__pharmacy, null);
        if (pharmacyInfo != null) {
            ((TextView) view.findViewById(R.id.tvLegalBusinessName)).setText(pharmacyInfo.getLegal_business_name());
            ((TextView) view.findViewById(R.id.tvDoingBusinessAs)).setText(pharmacyInfo.getDoing_business_as());
            ((TextView) view.findViewById(R.id.tvShippingAddress1)).setText(pharmacyInfo.getShipping_address1());
            ((TextView) view.findViewById(R.id.tvShippingAddress2)).setText(pharmacyInfo.getShipping_address2());
            ((TextView) view.findViewById(R.id.tvShippingCity)).setText(pharmacyInfo.getShipping_city());
            ((TextView) view.findViewById(R.id.tvShippingState)).setText(pharmacyInfo.getShipping_state());
            ((TextView) view.findViewById(R.id.tvShippingZipCode)).setText(pharmacyInfo.getShipping_zip_code());
            ((TextView) view.findViewById(R.id.tvPhone)).setText(pharmacyInfo.getPhone());
            ((TextView) view.findViewById(R.id.tvFax)).setText(pharmacyInfo.getFax());
            ((TextView) view.findViewById(R.id.tvMailingAddress1)).setText(pharmacyInfo.getMailing_address1());
            ((TextView) view.findViewById(R.id.tvMailingAddress2)).setText(pharmacyInfo.getMailing_address2());
            ((TextView) view.findViewById(R.id.tvMailingCity)).setText(pharmacyInfo.getMailing_city());
            ((TextView) view.findViewById(R.id.tvMailingState)).setText(pharmacyInfo.getMailing_state());
            ((TextView) view.findViewById(R.id.tvMailingZipCode)).setText(pharmacyInfo.getMailing_zip_code());
        }

        return view;
    }
}
