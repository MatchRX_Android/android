package com.htcindia.matchrx_30.fragment.sell;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.adapter.SellViewPagerAdapter;
import com.htcindia.matchrx_30.listeners.ScannerListener;
import com.htcindia.matchrx_30.listeners.SnackBarListener;
import com.htcindia.matchrx_30.widgets.CViewPager;


public class SellFragment extends Fragment {

    private SellViewPagerAdapter sellViewPagerAdapter;
    private CViewPager viewPager;
    private TabLayout tabLayout;
    private ScannerListener scannerListener;
    private SnackBarListener snackBarListener;

    private MyPostingFragment myPostingFragment;
    private PostNewItemFragment postNewItemFragment;

    public SellFragment() {
        // Required empty public constructor
    }

    public void setScannerListener(ScannerListener scannerListener) {
        this.scannerListener = scannerListener;
    }

    public void setSnackBarListener(SnackBarListener snackBarListener) {
        this.snackBarListener = snackBarListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myPostingFragment = new MyPostingFragment();
        postNewItemFragment = new PostNewItemFragment();
        postNewItemFragment.setScannerListener(scannerListener);
        postNewItemFragment.setSnackBarListener(snackBarListener);
    }

    public void searchNDCDrug(String ndcNo) {
        ((PostNewItemFragment) sellViewPagerAdapter.getItem(0)).searchNDCDrug(ndcNo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.sell));

        View view = inflater.inflate(R.layout.fragment_sell, container, false);

        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);

        sellViewPagerAdapter = new SellViewPagerAdapter(getFragmentManager());
        sellViewPagerAdapter.addFragment(postNewItemFragment, "Post New Item");
        sellViewPagerAdapter.addFragment(myPostingFragment, "My Posting");

        viewPager.setAdapter(sellViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        scannerListener = null;
        snackBarListener=null;
    }


}
