package com.htcindia.matchrx_30.fragment.myorders;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.adapter.MyOrdersAdapter;
import com.htcindia.matchrx_30.adapter.MyPurchaseAdapter;
import com.htcindia.matchrx_30.utils.Constants;

public class MyOrdersFragment extends Fragment {

    MyOrdersAdapter myOrdersAdapter;
    MyPurchaseAdapter myPurchaseAdapter;

    ListView listView;
    TextView tvSales;
    TextView tvPurchases;
    TextView tvHistory;

    public MyOrdersFragment() {
        // Required empty public constructor
    }


    public static MyOrdersFragment newInstance(String param1, String param2) {
        MyOrdersFragment fragment = new MyOrdersFragment();

        return fragment;
    }

    String landingPageType;

    public void setLandingPageType(String landingPageType) {
        this.landingPageType = landingPageType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.my_orders));

        myOrdersAdapter = new MyOrdersAdapter(getActivity());
        myPurchaseAdapter = new MyPurchaseAdapter(getActivity());

        View view = inflater.inflate(R.layout.fragment_my_orders, null);

        listView = view.findViewById(R.id.listView);
        tvSales = view.findViewById(R.id.tvSales);
        tvPurchases = view.findViewById(R.id.tvPurchase);
        tvHistory = view.findViewById(R.id.tvHistory);

        tvSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                highlightSales();
                unhighlightPurchases();
                unhighlightHistory();
                listView.setAdapter(myOrdersAdapter);
            }
        });


        tvPurchases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                highlightPurchases();
                unhighlightSales();
                unhighlightHistory();
                listView.setAdapter(myPurchaseAdapter);
            }
        });

        tvHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                highlightHistory();
                unhighlightSales();
                unhighlightPurchases();
                listView.setAdapter(myPurchaseAdapter);
            }
        });


        if (landingPageType.equals(Constants.SELLER_CONFIRMATION)) {
            highlightSales();
            listView.setAdapter(myOrdersAdapter);
        } else if (landingPageType.equals(Constants.BUYER_CONFIRMATION)) {
            highlightPurchases();
            listView.setAdapter(myPurchaseAdapter);
        }

        return view;
    }

    private void highlightSales() {
        tvSales.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.tab_sales_highlighted));
        tvSales.setTextColor(Color.WHITE);
    }

    private void unhighlightSales() {
        tvSales.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.tab_sales_unhighlighted));
        tvSales.setTextColor(Color.BLACK);
    }

    private void highlightPurchases() {
        tvPurchases.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.tab_purchase_highlighted));
        tvPurchases.setTextColor(Color.WHITE);
    }

    private void unhighlightPurchases() {
        tvPurchases.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.tab_purchase_unhighlighted));
        tvPurchases.setTextColor(Color.BLACK);
    }

    private void highlightHistory() {
        listView.setVisibility(View.GONE);
        tvHistory.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.tab_history_highlighted));
        tvHistory.setTextColor(Color.WHITE);
    }

    private void unhighlightHistory() {
        listView.setVisibility(View.VISIBLE);
        tvHistory.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.tab_history_unhighlighted));
        tvHistory.setTextColor(Color.BLACK);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
