package com.htcindia.matchrx_30.fragment.sell;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.fragment.BaseFragment;
import com.htcindia.matchrx_30.listeners.OnMonthPickedListener;
import com.htcindia.matchrx_30.listeners.ScannerListener;
import com.htcindia.matchrx_30.listeners.SnackBarListener;
import com.htcindia.matchrx_30.model.MatchRxDetails;
import com.htcindia.matchrx_30.model.MediSpanDetails;
import com.htcindia.matchrx_30.model.response.NDCVerificationResp;
import com.htcindia.matchrx_30.utils.Constants;
import com.htcindia.matchrx_30.utils.MonthYearPicker;
import com.htcindia.matchrx_30.utils.NDC;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EnterNDCFragment extends SellSupportFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener {

    public EditText etNDCNo;
    View ivSearch;
    TextView tvExpDate;
    View scan;
    View llNDCDrugInfo;
    ImageView ivMediSpanDrugPic;

    private ScannerListener scannerListener;
    private SnackBarListener snackBarListener;

    View llImageUpload1;
    View llImageUpload2;
    boolean isUploadImage1;
    boolean isUploadImage2;
    View flDrugPic1;
    View flDrugPic2;
    ImageView ivDrugPic1;
    ImageView ivDrugPic2;
    View ivPicDelete1;
    View ivPicDelete2;

    EditText etSellerNotes;
    EditText etPartialCount;
    EditText etLotNumber;

    RadioButton rbFull;
    RadioButton rbPartial;
    RadioButton rbSealed;
    RadioButton rbNonSealed;
    RadioGroup rgPackQuantity;
    RadioGroup rgOriginalPackage;

    CheckBox cbTornPackage;
    CheckBox cbSticker;
    CheckBox cbXcontainer;
    CheckBox cbWriting;
    CheckBox cbOther;

    Spinner spPackageAvailable;

    TextView tvDrugName;
    TextView tvSimilarItemsCount;
    TextView tvStrength;
    TextView tvPackaging;
    TextView tvStorage;
    TextView tvForm;
    TextView tvPackageUnit;
    TextView tvPartialMaxCount;

    TextView tvWaringMsg1;
    TextView tvWaringMsg2;

    public EnterNDCFragment() {
    }

    public void setScannerListener(ScannerListener scannerListener) {
        this.scannerListener = scannerListener;
    }

    public void setSnackBarListener(SnackBarListener snackBarListener) {
        this.snackBarListener = snackBarListener;
    }

    public void searchNDCDrug(String ndcNo) {
        etNDCNo.setText(ndcNo);
        etNDCNo.setSelection(etNDCNo.getText().length());
        if (validateNDC(ndcNo)) {
            if (!ndcNo.equalsIgnoreCase(NDC.PHENADOZ_NDC) && !ndcNo.equalsIgnoreCase(NDC.CARBAMAZEPINE_ER_NDC)
                    && !ndcNo.equalsIgnoreCase(NDC.DYRENIUM_NDC) && !ndcNo.equalsIgnoreCase(NDC.CELLCEPT_NDC)) {
                snackBarListener.showSnackBar("Sorry, this NDC details are not available with us. Please try another NDC.");
                hideKeyBoard();
                return;
            }
            updateView(loadNDCVerificationData(ndcNo));
        } else {
            snackBarListener.showSnackBar(getString(R.string.ndc_validation_msg));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_enter_ndc, container, false);
        init(view);

        etLotNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() >= 1) {
                    fieldEnabledState(true);
                } else if (s.toString().length() == 0) {
                    fieldEnabledState(false);
                }
            }
        });

        etPartialCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rbPartial.setChecked(true);
                }
            }
        });

//        etSellerNotes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    cbOther.setChecked(true);
//                }
//            }
//        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        scannerListener = null;
        snackBarListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                File file = new File(resultUri.getPath());

                if (isUploadImage1) {
                    Picasso.with(getActivity()).load(resultUri).fit().into(ivDrugPic1);
                    llImageUpload1.setVisibility(View.GONE);
                    flDrugPic1.setVisibility(View.VISIBLE);
                } else {
                    Picasso.with(getActivity()).load(resultUri).fit().into(ivDrugPic2);
                    llImageUpload2.setVisibility(View.GONE);
                    flDrugPic2.setVisibility(View.VISIBLE);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            rbSealed.setChecked(true);
            rbNonSealed.setEnabled(false);
            if (Constants.STATE_WISE_RESTRICTION.equals(Constants.BOTH_ALLOWED)) {
                tvWaringMsg2.setVisibility(View.VISIBLE);
                tvWaringMsg2.setText("NOTE: MatchRX policy limits the sale of any full unsealed NDC to one (1) every 90 days.");
            }

        } else {
            if (rbFull.isChecked() && (etLotNumber.getText().length() > 0)) {
                rbNonSealed.setEnabled(true);
                tvWaringMsg2.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.scan:
                scannerListener.startScanner();
                break;

            case R.id.ivSearch:
                String ndcNo = etNDCNo.getText().toString();

                if (validateNDC(ndcNo)) {
                    if (!ndcNo.equalsIgnoreCase(NDC.PHENADOZ_NDC) && !ndcNo.equalsIgnoreCase(NDC.CARBAMAZEPINE_ER_NDC)
                            && !ndcNo.equalsIgnoreCase(NDC.DYRENIUM_NDC) && !ndcNo.equalsIgnoreCase(NDC.CELLCEPT_NDC)) {
                        snackBarListener.showSnackBar("Sorry, this NDC details are not available with us. Please try another NDC.");
                        hideKeyBoard();
                        return;
                    }
                    updateView(loadNDCVerificationData(ndcNo));
                } else {
                    snackBarListener.showSnackBar(getString(R.string.ndc_validation_msg));
                }
                hideKeyBoard();
                break;

            case R.id.tvExpDate:
                MonthYearPicker monthYearPicker = new MonthYearPicker(new OnMonthPickedListener() {
                    @Override
                    public void SelectedYearMonth(int year, int month) {
                        String expDate = String.format(Locale.ENGLISH, "%02d", month) + "/" + year;
                        tvExpDate.setText(expDate);
                    }
                });
                monthYearPicker.setCancelable(false);
                monthYearPicker.show(getActivity().getSupportFragmentManager(), "month_year_picker");
                break;

            case R.id.llImageUpload1:
                callCropActivity();
//                imageUpload();
                isUploadImage1 = true;
                isUploadImage2 = false;
                break;

            case R.id.llImageUpload2:
                callCropActivity();
//                imageUpload();
                isUploadImage1 = false;
                isUploadImage2 = true;
                break;

            case R.id.ivPicDelete1:
                showDeletePicAlert(1);
                break;

            case R.id.ivPicDelete2:
                showDeletePicAlert(2);
                break;
        }
    }

    private void imageUpload() {

//        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("image/*.png");
//        intent.setType("image/*.jpg");
//        intent.setType("image/*.jpeg");
////        intent.setAction(Intent.ACTION_GET_CONTENT);
//
//
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        Intent chooserIntent = Intent.createChooser(intent, "Select");
//        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{cameraIntent});
//
//        startActivityForResult(chooserIntent, 2000);

//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2000);

        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*.jpeg");
        pickIntent.setType("image/*.png");
        pickIntent.setType("image/*.jpg");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent, 2000);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {

            case R.id.cbOther:
                if (buttonView.isChecked()) {
                    etSellerNotes.setEnabled(true);
                    etSellerNotes.setEnabled(true);
                } else {
                    etSellerNotes.clearFocus();
                    etSellerNotes.setEnabled(false);
//                    etSellerNotes.setEnabled(false);/
                }
                break;

            case R.id.cbWriting:
                break;

            case R.id.cbXcontainer:
                break;

            case R.id.cbSticker:
                break;

            case R.id.cbTornPackage:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbFull:
                onFullSelectValidations();
                break;

            case R.id.rbPartial:
                onPartialSelectValidations();
                break;

            case R.id.rbSealed:
                tvWaringMsg2.setVisibility(View.GONE);
                break;

            case R.id.rbNonSealed:
                spPackageAvailable.setSelection(0);
                tvWaringMsg2.setVisibility(View.GONE);
                break;
        }
    }

    private void updateView(NDCVerificationResp.NDCVerificationData ndcVerificationData) {

        llImageUpload1.setVisibility(View.VISIBLE);
        flDrugPic1.setVisibility(View.GONE);
        llImageUpload2.setVisibility(View.VISIBLE);
        flDrugPic2.setVisibility(View.GONE);

        etLotNumber.setText("");
        tvExpDate.setText("");
        etSellerNotes.setText("");
        etPartialCount.setText("");

        MediSpanDetails mediSpanDetails = ndcVerificationData.getMedispan_details();
        MatchRxDetails matchRxDetails = ndcVerificationData.getMatchrx_details();

        tvDrugName.setText(mediSpanDetails.getProduct_name());
        String similarItems = matchRxDetails.getSimilar_items() + " Similar items posted";
        tvSimilarItemsCount.setText(similarItems);

        MediSpanDetails.Packaging packaging = mediSpanDetails.getPackaging();
        String strength = packaging.getPackaging_extension().getPackage_strength() + " " + packaging.getPackaging_extension().getStrength_unit_of_measure();
        tvStrength.setText(strength);
        String packagingTxt = packaging.getPackage_type() + "(" + packaging.getPackage_size() + packaging.getPackage_unit_of_measure() + ")";
        tvPackaging.setText(packagingTxt);
        if (packaging.getKey_identifier().getStorage_condition_code().length() > 0) {
            tvStorage.setText(pickStorageType(packaging.getKey_identifier().getStorage_condition_code().charAt(0)));
        }
        tvForm.setText(packaging.getPackage_dosage_form());
        int partialMaxCount = Integer.parseInt(packaging.getPackage_size()) - 1;
        tvPartialMaxCount.setText("MAX :" + partialMaxCount);
        tvPackageUnit.setText(packaging.getPackage_unit_of_measure());
        if (packaging.getPackage_unit_of_measure().length() > 1) {
            populatePackAvailable(matchRxDetails.getPackage_available(), spPackageAvailable);
        }
        loadMediSpanDrugPic(mediSpanDetails.getNdc_number(), ivMediSpanDrugPic);

        llNDCDrugInfo.setVisibility(View.VISIBLE);

        fieldEnabledState(false);
    }


    private void onPartialSelectValidations() {
        rbPartial.setChecked(true);
        rbNonSealed.setChecked(true);
        rbSealed.setEnabled(false);
        spPackageAvailable.setSelection(0);
        spPackageAvailable.setEnabled(false);
        tvWaringMsg2.setVisibility(View.GONE);
    }

    private void onFullSelectValidations() {

        rbPartial.setChecked(false);
        etPartialCount.setText("");
        etPartialCount.clearFocus();
        spPackageAvailable.setSelection(0);
        spPackageAvailable.setClickable(false);
        if (Constants.STATE_WISE_RESTRICTION.equals(Constants.BOTH_ALLOWED)) {
            tvWaringMsg2.setVisibility(View.GONE);
            tvWaringMsg2.setText("Please select package condition.");
        }
        if (!Constants.STATE_WISE_RESTRICTION.equalsIgnoreCase(Constants.ONLY_NON_SEALED)) {
            rbSealed.setEnabled(true);
            rgOriginalPackage.clearCheck();
            spPackageAvailable.setClickable(true);
        }
    }

    private void callCropActivity() {
        if (getContext() != null) {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAllowRotation(false)
                    .setAllowFlipping(false)
                    .start(getContext(), this);
        }
    }

    private void fieldEnabledState(boolean enabled) {
        //Package Quantity
        rbFull.setEnabled(enabled);
        rbPartial.setEnabled(enabled);
        etPartialCount.setEnabled(enabled);

        //Package Available
        spPackageAvailable.setClickable(enabled);

        //Original Package
        rbSealed.setEnabled(enabled);
        rbNonSealed.setEnabled(enabled);

        //Package Condition
        cbTornPackage.setEnabled(enabled);
        cbSticker.setEnabled(enabled);
        cbWriting.setEnabled(enabled);
        cbXcontainer.setEnabled(enabled);
        cbOther.setEnabled(enabled);

        //Seller Notes
        etSellerNotes.setEnabled(enabled);

        if (!enabled) {
            cbTornPackage.setAlpha(0.5f);
            cbWriting.setAlpha(0.5f);
            cbWriting.setAlpha(0.5f);
            cbXcontainer.setAlpha(0.5f);
            cbSticker.setAlpha(0.5f);
            cbOther.setAlpha(0.5f);
            rbSealed.setAlpha(0.5f);
            rbNonSealed.setAlpha(0.5f);
            rbFull.setAlpha(0.5f);
            rbPartial.setAlpha(0.5f);
        }

        if (enabled) {
            cbTornPackage.setAlpha(1f);
            cbWriting.setAlpha(1f);
            cbWriting.setAlpha(1f);
            cbXcontainer.setAlpha(1f);
            cbSticker.setAlpha(1f);
            cbOther.setAlpha(1f);
            rbSealed.setAlpha(1f);
            rbNonSealed.setAlpha(1f);
            rbFull.setAlpha(1f);
            rbPartial.setAlpha(1f);
        }

        if (Constants.STATE_WISE_RESTRICTION.equalsIgnoreCase(Constants.ONLY_SEALED)) {
            rbPartial.setEnabled(false);
            etPartialCount.setEnabled(false);
            rbNonSealed.setEnabled(false);
            rbNonSealed.setAlpha(0.5f);
            rbPartial.setAlpha(0.5f);
        }

        if (Constants.STATE_WISE_RESTRICTION.equalsIgnoreCase(Constants.ONLY_NON_SEALED)) {
            rbSealed.setEnabled(false);
            rbSealed.setAlpha(0.5f);
            rbNonSealed.setChecked(true);
            spPackageAvailable.setClickable(false);
        }
    }

    public void init(View view) {

        etNDCNo = view.findViewById(R.id.etNDC);
        scan = view.findViewById(R.id.scan);
        ivSearch = view.findViewById(R.id.ivSearch);
        ivMediSpanDrugPic = view.findViewById(R.id.ivMediSpanDrugPic);
        tvExpDate = view.findViewById(R.id.tvExpDate);
        etSellerNotes = view.findViewById(R.id.etSellerNotes);
        etPartialCount = view.findViewById(R.id.etPartialCount);
        llNDCDrugInfo = view.findViewById(R.id.llNDCDrugInfo);
        etLotNumber = view.findViewById(R.id.etLotNumber);

        spPackageAvailable = view.findViewById(R.id.spPackageAvailable);

        //Image Upload
        llImageUpload1 = view.findViewById(R.id.llImageUpload1);
        llImageUpload2 = view.findViewById(R.id.llImageUpload2);
        flDrugPic1 = view.findViewById(R.id.flDrugPic1);
        flDrugPic2 = view.findViewById(R.id.flDrugPic2);
        ivDrugPic1 = view.findViewById(R.id.ivDrugPic1);
        ivDrugPic2 = view.findViewById(R.id.ivDrugPic2);
        ivPicDelete1 = view.findViewById(R.id.ivPicDelete1);
        ivPicDelete2 = view.findViewById(R.id.ivPicDelete2);

        rbFull = view.findViewById(R.id.rbFull);
        rbPartial = view.findViewById(R.id.rbPartial);
        rbSealed = view.findViewById(R.id.rbSealed);
        rbNonSealed = view.findViewById(R.id.rbNonSealed);

        rgOriginalPackage = view.findViewById(R.id.rgOriginalPackage);
        rgPackQuantity = view.findViewById(R.id.rgPackQuantity);

        cbTornPackage = view.findViewById(R.id.cbTornPackage);
        cbSticker = view.findViewById(R.id.cbSticker);
        cbXcontainer = view.findViewById(R.id.cbXcontainer);
        cbWriting = view.findViewById(R.id.cbWriting);
        cbOther = view.findViewById(R.id.cbOther);

        tvDrugName = view.findViewById(R.id.tvDrugName);
        tvSimilarItemsCount = view.findViewById(R.id.tvSimilarItemsCount);
        tvStrength = view.findViewById(R.id.tvStrength);
        tvPackaging = view.findViewById(R.id.tvPackaging);
        tvStorage = view.findViewById(R.id.tvStorage);
        tvForm = view.findViewById(R.id.tvForm);
        tvPackageUnit = view.findViewById(R.id.tvPackageUnit);
        tvPartialMaxCount = view.findViewById(R.id.tvPartialMaxCount);

        tvWaringMsg1 = view.findViewById(R.id.tvWaringMsg1);
        tvWaringMsg2 = view.findViewById(R.id.tvWaringMsg2);

        spPackageAvailable.setOnItemSelectedListener(this);

        scan.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        tvExpDate.setOnClickListener(this);
        llImageUpload1.setOnClickListener(this);
        llImageUpload2.setOnClickListener(this);
        ivPicDelete1.setOnClickListener(this);
        ivPicDelete2.setOnClickListener(this);

        cbOther.setOnCheckedChangeListener(this);
        cbWriting.setOnCheckedChangeListener(this);
        cbXcontainer.setOnCheckedChangeListener(this);
        cbSticker.setOnCheckedChangeListener(this);
        cbTornPackage.setOnCheckedChangeListener(this);

        rbFull.setOnCheckedChangeListener(this);
        rbPartial.setOnCheckedChangeListener(this);
        rbSealed.setOnCheckedChangeListener(this);
        rbNonSealed.setOnCheckedChangeListener(this);

        rgOriginalPackage.setOnCheckedChangeListener(this);
        rgPackQuantity.setOnCheckedChangeListener(this);
    }

    public void showDeletePicAlert(final int picPosition) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setMessage("Are you sure want to delete this image ? ");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (picPosition == 1) {
                    llImageUpload1.setVisibility(View.VISIBLE);
                    flDrugPic1.setVisibility(View.GONE);
                } else {
                    llImageUpload2.setVisibility(View.VISIBLE);
                    flDrugPic2.setVisibility(View.GONE);
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

}
