package com.htcindia.matchrx_30.fragment.myaccount;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htcindia.matchrx_30.LandingActivity;
import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.adapter.MyAccountViewPagerAdapter;
import com.htcindia.matchrx_30.model.Pharmacy;
import com.htcindia.matchrx_30.model.PharmacyInfo;
import com.htcindia.matchrx_30.model.response.PharmacyInfoResp;
import com.htcindia.matchrx_30.utils.Internet;
import com.htcindia.matchrx_30.webservice.OnWebResponse;
import com.htcindia.matchrx_30.webservice.WebRequest;

import static android.content.Context.MODE_PRIVATE;


public class PharmacyInfoFragment extends Fragment implements OnWebResponse {

    private View pharmacyInfoView;
    private View view;
    private MyAccountViewPagerAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private View snackBarView;

    public PharmacyInfoFragment() {

    }

    public void setView(View snackBarView) {
        this.snackBarView = snackBarView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.my_account));

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_pharmacy_info_frgament, container, false);
            pharmacyInfoView = view.findViewById(R.id.pharmacyInfoView);
            viewPager = view.findViewById(R.id.viewpager);

            tabLayout = view.findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);

            adapter = new MyAccountViewPagerAdapter(getFragmentManager());
            adapter.addFragment(new PharmacyFragment(), getString(R.string.pharmacy_tab_title));
            adapter.addFragment(new OwnerFragment(), getString(R.string.owner_tab_title));
            adapter.addFragment(new LicenseFragment(), getString(R.string.license_tab_title));
        }
        pharmacyInfoWebServiceCall(getArguments().getLong(getString(R.string.pharmacy_id)));
        return view;
    }

    private void updateView(PharmacyInfo pharmacyInfo) {

        //Header
        ((TextView) view.findViewById(R.id.tvPharmacyName)).setText(pharmacyInfo.getPharmacy().getPharmacy_information().getLegal_business_name());
        ;
        ((TextView) view.findViewById(R.id.tvOwnerName)).setText(pharmacyInfo.getFirst_name() + " " + pharmacyInfo.getLast_name());
        ((TextView) view.findViewById(R.id.tvDeaNo)).setText("DEA#  " + pharmacyInfo.getPharmacy().getPharmacy_license_info().getDea());
        ((TextView) view.findViewById(R.id.tvEmail)).setText(pharmacyInfo.getEmail());

        //Tabs
        ((PharmacyFragment) adapter.getItem(0)).setPharmacyInfo(pharmacyInfo.getPharmacy().getPharmacy_information());
        ((OwnerFragment) adapter.getItem(1)).setPharmacyOwnerInfo(pharmacyInfo.getPharmacy().getPharmacy_owner_info());
        ((LicenseFragment) adapter.getItem(2)).setPharmacyLicenseInfo(pharmacyInfo.getPharmacy().getPharmacy_license_info());
        viewPager.setAdapter(adapter);

        view.findViewById(R.id.overlay).setVisibility(View.GONE);
    }

    private void pharmacyInfoWebServiceCall(long pharmacyId) {
        if (Internet.isConnected(getActivity(), true, pharmacyInfoView)) {
            ((LandingActivity) getActivity()).showLoadingView();
            new WebRequest(getActivity(), this, WebRequest.Type.PHARMACY_INFO).PharmacyInfo(pharmacyId);
        }
    }

    @Override
    public void onSuccess(Object response, int type) {
        PharmacyInfoResp pharmacyInfoResp = (PharmacyInfoResp) response;
        updateView(pharmacyInfoResp.getData());
        ((LandingActivity) getActivity()).hideLoadingView();
    }

    @Override
    public void onFailure(String reason, int type) {
        ((LandingActivity) getActivity()).hideLoadingView();
        ((LandingActivity) getActivity()).showSnackBar(pharmacyInfoView, reason);
    }
}
