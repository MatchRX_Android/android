package com.htcindia.matchrx_30.listeners;

/**
 * Created by sathishk on 12/7/2017.
 */

public interface ScannerListener {
    public void startScanner();
}
