package com.htcindia.matchrx_30.listeners;

/**
 * Created by sathishk on 12/15/2017.
 */

public interface SnackBarListener {

    public void showSnackBar(String msg);
}
