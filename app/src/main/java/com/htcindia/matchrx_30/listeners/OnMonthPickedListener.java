package com.htcindia.matchrx_30.listeners;

/**
 * Created by sathishk on 12/12/2017.
 */

public interface OnMonthPickedListener {
    public void SelectedYearMonth(int year, int month);
}
