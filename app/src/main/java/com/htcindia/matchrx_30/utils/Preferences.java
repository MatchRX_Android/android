package com.htcindia.matchrx_30.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sathishk on 11/23/2017.
 */

public class Preferences {

    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPrefers;

    public Preferences(Context paramContext) {
        this.sharedPrefers = paramContext.getSharedPreferences("MatchRxPref", Context.MODE_PRIVATE);
        this.editor = sharedPrefers.edit();
    }

    public void setUserLoggedInStatus(boolean status) {
        editor.putBoolean("logged_in", status);
        editor.commit();
    }

    public boolean getUserLoggedInStatus() {
        if (sharedPrefers.contains("logged_in")) {
            return sharedPrefers.getBoolean("logged_in", false);
        }
        return false;
    }


}
