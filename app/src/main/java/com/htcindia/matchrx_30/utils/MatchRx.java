package com.htcindia.matchrx_30.utils;

import android.app.Application;

import com.htcindia.matchrx_30.model.Token;

/**
 * Created by sathishk on 11/23/2017.
 */

public class MatchRx extends Application {

    public static Token TOKEN = null;
    public static Preferences PREF = null;


    @Override
    public void onCreate() {
        super.onCreate();
        PREF = new Preferences(this);
    }
}
