package com.htcindia.matchrx_30.utils;

/**
 * Created by sathishk on 12/14/2017.
 */

public class NDC {

    public static final String PHENADOZ_NDC = "00591-2985-39";
    public static final String DYRENIUM_NDC = "65197-0002-01";
    public static final String CARBAMAZEPINE_ER_NDC = "60505-2806-07";
    public static final String CELLCEPT_NDC = "00004-0259-01";

    public static final String PHENADOZ = "{\n" +
            "  \"code\": 200,\n" +
            "  \"status\": \"OK\",\n" +
            "  \"data\": {\n" +
            "    \"medispan_details\": {\n" +
            "      \"ndc_number\": \"00591-2985-39\",\n" +
            "      \"product_name\": \"PHENADOZ\",\n" +
            "      \"packaging\": {\n" +
            "        \"package_dosage_form\": \"SUPP\",\n" +
            "        \"package_size\": \"12\",\n" +
            "        \"package_unit_of_measure\": \"EA\",\n" +
            "        \"package_qty\": \"1\",\n" +
            "        \"actual_package_size\": \"12\",\n" +
            "        \"package_type\": \"BOX\",\n" +
            "        \"tot_package_qty\": \"12\",\n" +
            "        \"packaging_extension\": {\n" +
            "          \"package_strength\": \"12.5\",\n" +
            "          \"strength_unit_of_measure\": \"MG\"\n" +
            "        },\n" +
            "        \"price_details \": {\n" +
            "          \"package_price\": 20,\n" +
            "          \"unit_price\": 2,\n" +
            "          \"price_type\": \"wac/awp\"\n" +
            "        },\n" +
            "        \"key_identifier\": {\n" +
            "          \"formatted_id_number\": \"00591-2985-39\",\n" +
            "          \"storage_condition_code\": \"R\"\n" +
            "        },\n" +
            "        \"image\": \"url\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"matchrx_details\": {\n" +
            "      \"similar_items\": 44,\n" +
            "      \"is_brand\": \"true\",\n" +
            "      \"package_available\": 15,\n" +
            "      \"partialpackExists\": \"false\",\n" +
            "      \"partialpackMessage\": \"\",\n" +
            "      \"nonsealedpackExists\": \"false\",\n" +
            "      \"nonsealedpackerrorMessage\": \"\",\n" +
            "      \"minprice\": 1.23,\n" +
            "      \"maxprice\": 24.5,\n" +
            "      \"unit_price\": 24.5,\n" +
            "      \"min_discount\": 34,\n" +
            "      \"max_discount\": 50,\n" +
            "      \"min_exp_date\": \"\",\n" +
            "      \"max_exp_date\": \"\",\n" +
            "      \"wac_max_price\": 90,\n" +
            "      \"aawp_max_price\": 75,\n" +
            "      \"quantity_sold_message\": \"\",\n" +
            "      \"statewise_package_condition\": \"OnlySealed\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public static final String DYRENIUM = "{\n" +
            "  \"code\": 200,\n" +
            "  \"status\": \"OK\",\n" +
            "  \"data\": {\n" +
            "    \"medispan_details\": {\n" +
            "      \"ndc_number\": \"65197-0002-01\",\n" +
            "      \"product_name\": \"DYRENIUM\",\n" +
            "      \"packaging\": {\n" +
            "        \"package_dosage_form\": \"CAPS\",\n" +
            "        \"package_size\": \"100\",\n" +
            "        \"package_unit_of_measure\": \"EA\",\n" +
            "        \"package_qty\": \"1\",\n" +
            "        \"actual_package_size\": \"100\",\n" +
            "        \"package_type\": \"BOX\",\n" +
            "        \"tot_package_qty\": \"100\",\n" +
            "        \"packaging_extension\": {\n" +
            "          \"package_strength\": \"50\",\n" +
            "          \"strength_unit_of_measure\": \"MG\"\n" +
            "        },\n" +
            "        \"price_details \": {\n" +
            "          \"package_price\": 20,\n" +
            "          \"unit_price\": 2,\n" +
            "          \"price_type\": \"wac/awp\"\n" +
            "        },\n" +
            "        \"key_identifier\": {\n" +
            "          \"formatted_id_number\": \"65197-0002-01\",\n" +
            "          \"storage_condition_code\": \"N\"\n" +
            "        },\n" +
            "        \"image\": \"url\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"matchrx_details\": {\n" +
            "      \"similar_items\": 17,\n" +
            "      \"is_brand\": \"true\",\n" +
            "      \"package_available\": 15,\n" +
            "      \"partialpackExists\": \"false\",\n" +
            "      \"partialpackMessage\": \"\",\n" +
            "      \"nonsealedpackExists\": \"false\",\n" +
            "      \"nonsealedpackerrorMessage\": \"\",\n" +
            "      \"minprice\": 1.23,\n" +
            "      \"maxprice\": 24.5,\n" +
            "      \"unit_price\": 24.5,\n" +
            "      \"min_discount\": 34,\n" +
            "      \"max_discount\": 50,\n" +
            "      \"min_exp_date\": \"\",\n" +
            "      \"max_exp_date\": \"\",\n" +
            "      \"wac_max_price\": 90,\n" +
            "      \"aawp_max_price\": 75,\n" +
            "      \"quantity_sold_message\": \"\",\n" +
            "      \"statewise_package_condition\": \"OnlyNon-sealed\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public static final String CARBAMAZEPINE_ER = "{\n" +
            "  \"code\": 200,\n" +
            "  \"status\": \"OK\",\n" +
            "  \"data\": {\n" +
            "    \"medispan_details\": {\n" +
            "      \"ndc_number\": \"60505-2806-07\",\n" +
            "      \"product_name\": \"CARBAMAZEPINE ER\",\n" +
            "      \"packaging\": {\n" +
            "        \"package_dosage_form\": \"CP12\",\n" +
            "        \"package_size\": \"120\",\n" +
            "        \"package_unit_of_measure\": \"EA\",\n" +
            "        \"package_qty\": \"1\",\n" +
            "        \"actual_package_size\": \"120\",\n" +
            "        \"package_type\": \"BOX\",\n" +
            "        \"tot_package_qty\": \"120\",\n" +
            "        \"packaging_extension\": {\n" +
            "          \"package_strength\": \"200\",\n" +
            "          \"strength_unit_of_measure\": \"MG\"\n" +
            "        },\n" +
            "        \"price_details \": {\n" +
            "          \"package_price\": 20,\n" +
            "          \"unit_price\": 2,\n" +
            "          \"price_type\": \"wac/awp\"\n" +
            "        },\n" +
            "        \"key_identifier\": {\n" +
            "          \"formatted_id_number\": \"60505-2806-07\",\n" +
            "          \"storage_condition_code\": \"N\"\n" +
            "        },\n" +
            "        \"image\": \"url\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"matchrx_details\": {\n" +
            "      \"similar_items\": 70,\n" +
            "      \"is_brand\": \"false\",\n" +
            "      \"package_available\": 25,\n" +
            "      \"partialpackExists\": \"false\",\n" +
            "      \"partialpackMessage\": \"\",\n" +
            "      \"nonsealedpackExists\": \"false\",\n" +
            "      \"nonsealedpackerrorMessage\": \"\",\n" +
            "      \"minprice\": 1.23,\n" +
            "      \"maxprice\": 24.5,\n" +
            "      \"unit_price\": 24.5,\n" +
            "      \"min_discount\": 34,\n" +
            "      \"max_discount\": 50,\n" +
            "      \"min_exp_date\": \"\",\n" +
            "      \"max_exp_date\": \"\",\n" +
            "      \"wac_max_price\": 90,\n" +
            "      \"aawp_max_price\": 75,\n" +
            "      \"quantity_sold_message\": \"\",\n" +
            "      \"statewise_package_condition\": \"Bothallowed\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public static final String CELLCEPT = "{\n" +
            "  \"code\": 200,\n" +
            "  \"status\": \"OK\",\n" +
            "  \"data\": {\n" +
            "    \"medispan_details\": {\n" +
            "      \"ndc_number\": \"00004-0259-01\",\n" +
            "      \"product_name\": \"CELLCEPT\",\n" +
            "      \"packaging\": {\n" +
            "        \"package_dosage_form\": \"CAPS\",\n" +
            "        \"package_size\": \"100\",\n" +
            "        \"package_unit_of_measure\": \"EA\",\n" +
            "        \"package_qty\": \"1\",\n" +
            "        \"actual_package_size\": \"100\",\n" +
            "        \"package_type\": \"BOX\",\n" +
            "        \"tot_package_qty\": \"100\",\n" +
            "        \"packaging_extension\": {\n" +
            "          \"package_strength\": \"250\",\n" +
            "          \"strength_unit_of_measure\": \"MG\"\n" +
            "        },\n" +
            "        \"price_details \": {\n" +
            "          \"package_price\": 20,\n" +
            "          \"unit_price\": 2,\n" +
            "          \"price_type\": \"wac/awp\"\n" +
            "        },\n" +
            "        \"key_identifier\": {\n" +
            "          \"formatted_id_number\": \"00004-0259-01\",\n" +
            "          \"storage_condition_code\": \"N\"\n" +
            "        },\n" +
            "        \"image\": \"url\"\n" +
            "      }},\n" +
            "      \"matchrx_details\": {\n" +
            "        \"similar_items\": 6,\n" +
            "        \"is_brand\": \"false\",\n" +
            "        \"package_available\": 25,\n" +
            "         \"partialpackExists\": \"false\",\n" +
            "      \"partialpackMessage\": \"\",\n" +
            "      \"nonsealedpackExists\": \"false\",\n" +
            "      \"nonsealedpackerrorMessage\": \"\",\n" +
            "        \"minprice\": 1.23,\n" +
            "        \"maxprice\": 24.5,\n" +
            "        \"unit_price\": 24.5,\n" +
            "        \"min_discount\": 34,\n" +
            "        \"max_discount\": 50,\n" +
            "        \"min_exp_date\": \"\",\n" +
            "        \"max_exp_date\": \"\",\n" +
            "        \"wac_max_price\": 90,\n" +
            "        \"aawp_max_price\": 75,\n" +
            "        \"quantity_sold_message\": \"\",\n" +
            "        \"statewise_package_condition\": \"Bothallowed\"\n" +
            "      }\n" +
            "    }\n" +
            "  \n" +
            "}\n";
}
