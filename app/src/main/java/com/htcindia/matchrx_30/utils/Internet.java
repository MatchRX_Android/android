package com.htcindia.matchrx_30.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

/**
 * Created by sathishk on 11/8/2017.
 */

public class Internet {
    public static boolean isConnected(Context context, boolean needToast,View view) {
        try{
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connectivity != null){
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED){
                            return true;
                        }
            }

        }catch(Exception e){
            if(needToast)
                Snackbar.make(view,"Network error",Snackbar.LENGTH_SHORT).show();
        }

        if(needToast)
            Snackbar.make(view,"Kindly check your internet connectivity.",Snackbar.LENGTH_SHORT).show();

        return false;
    }
}
