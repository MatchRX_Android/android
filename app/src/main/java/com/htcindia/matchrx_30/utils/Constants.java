package com.htcindia.matchrx_30.utils;

/**
 * Created by sathishk on 11/8/2017.
 */

public class Constants {
    public static String SELLER_CONFIRMATION = "seller_confirmation";
    public static String BUYER_CONFIRMATION = "buyer_confirmation";
    public static String MARKETPLACE = "Marketplace ";

    public static int SCANNING_REQ_CODE = 1001;

    public static final int PERMISSION_REQUEST_CAMERA = 2001;

    public static String STATE_WISE_RESTRICTION = "Bothallowed";

    public static final String ONLY_SEALED = "OnlySealed";
    public static final String ONLY_NON_SEALED = "OnlyNon-sealed";
    public static final String BOTH_ALLOWED = "Bothallowed";




}
