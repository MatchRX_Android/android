package com.htcindia.matchrx_30.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.adapter.MonthAdapter;
import com.htcindia.matchrx_30.adapter.YearAdapter;
import com.htcindia.matchrx_30.listeners.OnMonthPickedListener;

import java.util.Calendar;

/**
 * Created by sathishk on 12/12/2017.
 */

public class MonthYearPicker extends DialogFragment {

    ViewFlipper vfCalendar;
    View view;

    YearAdapter yearAdapter;
    MonthAdapter monthAdapter;

    OnMonthPickedListener onMonthPickedListener;

    int selectedYear;
    int selectedMonth;

    Calendar calendar;

    public MonthYearPicker() {

    }

    @SuppressLint("ValidFragment")
    public MonthYearPicker(OnMonthPickedListener onMonthPickedListener) {
        this.onMonthPickedListener = onMonthPickedListener;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout((int) (ScreenSize.Width * 0.95), (int) (ScreenSize.Height * 0.46));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        yearAdapter = new YearAdapter(getActivity());
        monthAdapter = new MonthAdapter(getActivity());
        calendar = Calendar.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        view = inflater.inflate(R.layout.sell_calendar_dialog_layout, null);

        vfCalendar = view.findViewById(R.id.vfCalendar);
        View ivBack = view.findViewById(R.id.ivBack);
        GridView gvYear = view.findViewById(R.id.gvYear);
        GridView gvMonth = view.findViewById(R.id.gvMonth);

        gvYear.setNumColumns(3);
        gvMonth.setNumColumns(3);

        gvYear.setAdapter(yearAdapter);
        gvMonth.setAdapter(monthAdapter);

        gvYear.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateMonthPicker(((TextView) view.findViewById(R.id.calendarTxt)));
                vfCalendar.setDisplayedChild(1);
            }
        });

        gvMonth.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedMonth = position + 1;
                if (calendar.get(Calendar.YEAR) == selectedYear) {
                    if (position >= calendar.get(Calendar.MONTH)) {
                        MonthYearPicker.this.dismiss();
                    }
                } else {
                    MonthYearPicker.this.dismiss();
                }
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vfCalendar.setDisplayedChild(0);
            }
        });

        return view;
    }

    public void updateMonthPicker(TextView yearText) {
        selectedYear = Integer.parseInt(yearText.getText().toString());
        String selectedYearTxt = yearText.getText().toString();
        TextView tvYear = view.findViewById(R.id.tvYear);
        tvYear.setText(selectedYearTxt);

        if (calendar.get(Calendar.YEAR) == selectedYear) {
            monthAdapter.isCurrentYear(true);
            monthAdapter.notifyDataSetChanged();
        } else {
            monthAdapter.isCurrentYear(false);
            monthAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        onMonthPickedListener.SelectedYearMonth(selectedYear, selectedMonth);
        super.onDismiss(dialog);
    }
}
