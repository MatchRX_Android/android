package com.htcindia.matchrx_30.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import com.htcindia.matchrx_30.R;

/**
 * Created by sathishk on 11/21/2017.
 */

public class LoadingView extends RelativeLayout {
    public LoadingView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.loading_view, this,
                true);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }
}
