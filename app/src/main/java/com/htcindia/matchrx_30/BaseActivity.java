package com.htcindia.matchrx_30;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.htcindia.matchrx_30.utils.LoadingView;

/**
 * Created by sathishk on 11/14/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected LoadingView mLoadingView;

    public void showSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    protected void showAlert(String message, Context context) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    protected void showLogoutAlert(String message, final Context context) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    public void showLoadingView() {
        if (BaseActivity.this != null)

            if (mLoadingView != null) {
                hideLoadingView();
            }
        mLoadingView = new LoadingView(BaseActivity.this);
        ViewGroup decor = (ViewGroup) BaseActivity.this.getWindow().getDecorView();
        decor.addView(mLoadingView);
    }

    public void hideLoadingView() {
        try {
            if (mLoadingView == null)
                return;
            ((ViewGroup) (mLoadingView.getParent())).removeView(mLoadingView);
            mLoadingView = null;
        } catch (Exception e) {
        }
    }

    public void hideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
