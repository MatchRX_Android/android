package com.htcindia.matchrx_30.model;

/**
 * Created by sathishk on 12/14/2017.
 */

public class MatchRxDetails {

    private Integer similar_items;
    private boolean is_brand;
    private Integer package_available;
    private boolean partialpackExists;
    private String partialpackMessage;
    private boolean nonsealedpackExists;
    private String nonsealedpackerrorMessage;
    private Double minprice;
    private Double maxprice;
    private Double unit_price;
    private Double min_discount;
    private Double max_discount;
    private String min_exp_date;
    private String max_exp_date;
    private Integer wac_max_price;
    private Integer aawp_max_price;
    private String quantity_sold_message;
    private String statewise_package_condition;

    public Integer getSimilar_items() {
        return similar_items;
    }

    public void setSimilar_items(Integer similar_items) {
        this.similar_items = similar_items;
    }

    public boolean isIs_brand() {
        return is_brand;
    }

    public void setIs_brand(boolean is_brand) {
        this.is_brand = is_brand;
    }

    public Integer getPackage_available() {
        return package_available;
    }

    public void setPackage_available(Integer package_available) {
        this.package_available = package_available;
    }

    public boolean isPartialpackExists() {
        return partialpackExists;
    }

    public void setPartialpackExists(boolean partialpackExists) {
        this.partialpackExists = partialpackExists;
    }

    public String getPartialpackMessage() {
        return partialpackMessage;
    }

    public void setPartialpackMessage(String partialpackMessage) {
        this.partialpackMessage = partialpackMessage;
    }

    public boolean isNonsealedpackExists() {
        return nonsealedpackExists;
    }

    public void setNonsealedpackExists(boolean nonsealedpackExists) {
        this.nonsealedpackExists = nonsealedpackExists;
    }

    public String getNonsealedpackerrorMessage() {
        return nonsealedpackerrorMessage;
    }

    public void setNonsealedpackerrorMessage(String nonsealedpackerrorMessage) {
        this.nonsealedpackerrorMessage = nonsealedpackerrorMessage;
    }

    public Double getMinprice() {
        return minprice;
    }

    public void setMinprice(Double minprice) {
        this.minprice = minprice;
    }

    public Double getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(Double maxprice) {
        this.maxprice = maxprice;
    }

    public Double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(Double unit_price) {
        this.unit_price = unit_price;
    }

    public Double getMin_discount() {
        return min_discount;
    }

    public void setMin_discount(Double min_discount) {
        this.min_discount = min_discount;
    }

    public Double getMax_discount() {
        return max_discount;
    }

    public void setMax_discount(Double max_discount) {
        this.max_discount = max_discount;
    }

    public String getMin_exp_date() {
        return min_exp_date;
    }

    public void setMin_exp_date(String min_exp_date) {
        this.min_exp_date = min_exp_date;
    }

    public String getMax_exp_date() {
        return max_exp_date;
    }

    public void setMax_exp_date(String max_exp_date) {
        this.max_exp_date = max_exp_date;
    }

    public Integer getWac_max_price() {
        return wac_max_price;
    }

    public void setWac_max_price(Integer wac_max_price) {
        this.wac_max_price = wac_max_price;
    }

    public Integer getAawp_max_price() {
        return aawp_max_price;
    }

    public void setAawp_max_price(Integer aawp_max_price) {
        this.aawp_max_price = aawp_max_price;
    }

    public String getQuantity_sold_message() {
        return quantity_sold_message;
    }

    public void setQuantity_sold_message(String quantity_sold_message) {
        this.quantity_sold_message = quantity_sold_message;
    }

    public String getStatewise_package_condition() {
        return statewise_package_condition;
    }

    public void setStatewise_package_condition(String statewise_package_condition) {
        this.statewise_package_condition = statewise_package_condition;
    }
}
