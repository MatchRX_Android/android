package com.htcindia.matchrx_30.model.response;

import com.htcindia.matchrx_30.model.MatchRxDetails;
import com.htcindia.matchrx_30.model.MediSpanDetails;
import com.htcindia.matchrx_30.model.Status;

/**
 * Created by sathishk on 12/14/2017.
 */

public class NDCVerificationResp extends Status {

    private NDCVerificationData data;

    public NDCVerificationData getData() {
        return data;
    }

    public void setData(NDCVerificationData data) {
        this.data = data;
    }

    public class NDCVerificationData {
        private MediSpanDetails medispan_details;
        private MatchRxDetails matchrx_details;

        public MediSpanDetails getMedispan_details() {
            return medispan_details;
        }

        public void setMedispan_details(MediSpanDetails medispan_details) {
            this.medispan_details = medispan_details;
        }

        public MatchRxDetails getMatchrx_details() {
            return matchrx_details;
        }

        public void setMatchrx_details(MatchRxDetails matchrx_details) {
            this.matchrx_details = matchrx_details;
        }
    }
}
