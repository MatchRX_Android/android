package com.htcindia.matchrx_30.model;

/**
 * Created by sathishk on 11/21/2017.
 */

public class Pharmacy {

    private Integer pharmacy_id;

    private String legal_business_name = "";

    private String dea = "";

    private String title = "";

    private String pharmacy_software = "";

    private String enterprise_type = "";

    private String wholesaler_name_primary = "";

    private String wholesaler_primary_other = "";

    private String wholesaler_name_secondary = "";

    private String pharmacy_type = "";

    private String promo_code = "";

    private String hear_by = "";

    private String referred_by = "";

    private String number_of_location = "";

    private Integer vm_buyer_address_id;

    private String ip_address = "";

    private String store_flag = "";

    private PharmacyInformation pharmacy_information = new PharmacyInformation();

    private PharmacyOwnerInformation pharmacy_owner_info = new PharmacyOwnerInformation();

    private PharmacyLicenseInfo pharmacy_license_info = new PharmacyLicenseInfo();

    private PharmacyBankInfo pharmacy_bank_info = new PharmacyBankInfo();

    public String getLegal_business_name() {
        return legal_business_name;
    }

    public void setLegal_business_name(String legal_business_name) {
        this.legal_business_name = legal_business_name;
    }

    public String getDea() {
        return dea;
    }

    public void setDea(String dea) {
        this.dea = dea;
    }

    public PharmacyInformation getPharmacy_information() {
        return pharmacy_information;
    }

    public void setPharmacy_information(PharmacyInformation pharmacy_information) {
        this.pharmacy_information = pharmacy_information;
    }

    public PharmacyOwnerInformation getPharmacy_owner_info() {
        return pharmacy_owner_info;
    }

    public void setPharmacy_owner_info(PharmacyOwnerInformation pharmacy_owner_info) {
        this.pharmacy_owner_info = pharmacy_owner_info;
    }

    public PharmacyLicenseInfo getPharmacy_license_info() {
        return pharmacy_license_info;
    }

    public void setPharmacy_license_info(PharmacyLicenseInfo pharmacy_license_info) {
        this.pharmacy_license_info = pharmacy_license_info;
    }

    public PharmacyBankInfo getPharmacy_bank_info() {
        return pharmacy_bank_info;
    }

    public void setPharmacy_bank_info(PharmacyBankInfo pharmacy_bank_info) {
        this.pharmacy_bank_info = pharmacy_bank_info;
    }

    public Integer getPharmacy_id() {
        return pharmacy_id;
    }

    public void setPharmacy_id(Integer pharmacy_id) {
        this.pharmacy_id = pharmacy_id;
    }

    public String getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(String enterprise_type) {
        this.enterprise_type = enterprise_type;
    }

    public String getWholesaler_name_primary() {
        return wholesaler_name_primary;
    }

    public void setWholesaler_name_primary(String wholesaler_name_primary) {
        this.wholesaler_name_primary = wholesaler_name_primary;
    }

    public String getWholesaler_primary_other() {
        return wholesaler_primary_other;
    }

    public void setWholesaler_primary_other(String wholesaler_primary_other) {
        this.wholesaler_primary_other = wholesaler_primary_other;
    }

    public String getWholesaler_name_secondary() {
        return wholesaler_name_secondary;
    }

    public void setWholesaler_name_secondary(String wholesaler_name_secondary) {
        this.wholesaler_name_secondary = wholesaler_name_secondary;
    }

    public String getPharmacy_type() {
        return pharmacy_type;
    }

    public void setPharmacy_type(String pharmacy_type) {
        this.pharmacy_type = pharmacy_type;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public String getHear_by() {
        return hear_by;
    }

    public void setHear_by(String hear_by) {
        this.hear_by = hear_by;
    }

    public String getReferred_by() {
        return referred_by;
    }

    public void setReferred_by(String referred_by) {
        this.referred_by = referred_by;
    }

    public String getNumber_of_location() {
        return number_of_location;
    }

    public void setNumber_of_location(String number_of_location) {
        this.number_of_location = number_of_location;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getStore_flag() {
        return store_flag;
    }

    public void setStore_flag(String store_flag) {
        this.store_flag = store_flag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPharmacy_software() {
        return pharmacy_software;
    }

    public void setPharmacy_software(String pharmacy_software) {
        this.pharmacy_software = pharmacy_software;
    }

    public Integer getVm_buyer_address_id() {
        return vm_buyer_address_id;
    }

    public void setVm_buyer_address_id(Integer vm_buyer_address_id) {
        this.vm_buyer_address_id = vm_buyer_address_id;
    }


    public class PharmacyInformation {

        private String legal_business_name = "";

        private String doing_business_as = "";

        private String shipping_address1 = "";

        private String shipping_address2 = "";

        private String shipping_city = "";

        private String shipping_state = "";

        private String shipping_zip_code = "";

        private String phone = "";

        private String fax = "";

        private String mailing_address1 = "";

        private String mailing_address2 = "";

        private String mailing_city = "";

        private String mailing_state = "";

        private String mailing_zip_code = "";


        public String getLegal_business_name() {
            return legal_business_name;
        }

        public void setLegal_business_name(String legal_business_name) {
            this.legal_business_name = legal_business_name;
        }

        public String getDoing_business_as() {
            return doing_business_as;
        }

        public void setDoing_business_as(String doing_business_as) {
            this.doing_business_as = doing_business_as;
        }

        public String getShipping_address1() {
            return shipping_address1;
        }

        public void setShipping_address1(String shipping_address1) {
            this.shipping_address1 = shipping_address1;
        }

        public String getShipping_address2() {
            return shipping_address2;
        }

        public void setShipping_address2(String shipping_address2) {
            this.shipping_address2 = shipping_address2;
        }

        public String getShipping_city() {
            return shipping_city;
        }

        public void setShipping_city(String shipping_city) {
            this.shipping_city = shipping_city;
        }

        public String getShipping_state() {
            return shipping_state;
        }

        public void setShipping_state(String shipping_state) {
            this.shipping_state = shipping_state;
        }

        public String getShipping_zip_code() {
            return shipping_zip_code;
        }

        public void setShipping_zip_code(String shipping_zip_code) {
            this.shipping_zip_code = shipping_zip_code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getMailing_address1() {
            return mailing_address1;
        }

        public void setMailing_address1(String mailing_address1) {
            this.mailing_address1 = mailing_address1;
        }

        public String getMailing_address2() {
            return mailing_address2;
        }

        public void setMailing_address2(String mailing_address2) {
            this.mailing_address2 = mailing_address2;
        }

        public String getMailing_city() {
            return mailing_city;
        }

        public void setMailing_city(String mailing_city) {
            this.mailing_city = mailing_city;
        }

        public String getMailing_state() {
            return mailing_state;
        }

        public void setMailing_state(String mailing_state) {
            this.mailing_state = mailing_state;
        }

        public String getMailing_zip_code() {
            return mailing_zip_code;
        }

        public void setMailing_zip_code(String mailing_zip_code) {
            this.mailing_zip_code = mailing_zip_code;
        }
    }

    public class PharmacyOwnerInformation {

        private String owner_first_name = "";

        private String owner_last_name = "";

        private String owner_email = "";

        private String owner_mobile = "";

        public String getOwner_first_name() {
            return owner_first_name;
        }

        public void setOwner_first_name(String owner_first_name) {
            this.owner_first_name = owner_first_name;
        }

        public String getOwner_last_name() {
            return owner_last_name;
        }

        public void setOwner_last_name(String owner_last_name) {
            this.owner_last_name = owner_last_name;
        }

        public String getOwner_email() {
            return owner_email;
        }

        public void setOwner_email(String owner_email) {
            this.owner_email = owner_email;
        }

        public String getOwner_mobile() {
            return owner_mobile;
        }

        public void setOwner_mobile(String owner_mobile) {
            this.owner_mobile = owner_mobile;
        }
    }

    public class PharmacyLicenseInfo {

        private Long npi_number;

        private String ncpdp_number = "";

        private String dea = "";

        private String dea_exp_date = "";

        private String state_license_number = "";

        private String state_license_exp_date = "";

        public Long getNpi_number() {
            return npi_number;
        }

        public void setNpi_number(Long npi_number) {
            this.npi_number = npi_number;
        }

        public String getNcpdp_number() {
            return ncpdp_number;
        }

        public void setNcpdp_number(String ncpdp_number) {
            this.ncpdp_number = ncpdp_number;
        }

        public String getDea() {
            return dea;
        }

        public void setDea(String dea) {
            this.dea = dea;
        }

        public String getDea_exp_date() {
            return dea_exp_date;
        }

        public void setDea_exp_date(String dea_exp_date) {
            this.dea_exp_date = dea_exp_date;
        }

        public String getState_license_number() {
            return state_license_number;
        }

        public void setState_license_number(String state_license_number) {
            this.state_license_number = state_license_number;
        }

        public String getState_license_exp_date() {
            return state_license_exp_date;
        }

        public void setState_license_exp_date(String state_license_exp_date) {
            this.state_license_exp_date = state_license_exp_date;
        }
    }

    public class PharmacyBankInfo {

        private String bank_name = "";

        private String bank_account_numer = "";

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getBank_account_numer() {
            return bank_account_numer;
        }

        public void setBank_account_numer(String bank_account_numer) {
            this.bank_account_numer = bank_account_numer;
        }
    }
}
