package com.htcindia.matchrx_30.model;

/**
 * Created by sathishk on 12/14/2017.
 */

public class MediSpanDetails {

    private String ndc_number;
    private String product_name;
    private Packaging packaging;

    public String getNdc_number() {
        return ndc_number;
    }

    public void setNdc_number(String ndc_number) {
        this.ndc_number = ndc_number;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }

    public class Packaging {

        private String package_dosage_form;
        private String package_size;
        private String package_unit_of_measure;
        private String package_qty;
        private String actual_package_size;
        private String package_type;
        private String tot_package_qty;
        private PackageExtension packaging_extension;
        private PriceDetails price_details;
        private KeyIdentifier key_identifier;
        private String image;

        public String getPackage_dosage_form() {
            return package_dosage_form;
        }

        public void setPackage_dosage_form(String package_dosage_form) {
            this.package_dosage_form = package_dosage_form;
        }

        public String getPackage_size() {
            return package_size;
        }

        public void setPackage_size(String package_size) {
            this.package_size = package_size;
        }

        public String getPackage_unit_of_measure() {
            return package_unit_of_measure;
        }

        public void setPackage_unit_of_measure(String package_unit_of_measure) {
            this.package_unit_of_measure = package_unit_of_measure;
        }

        public String getPackage_qty() {
            return package_qty;
        }

        public void setPackage_qty(String package_qty) {
            this.package_qty = package_qty;
        }

        public String getActual_package_size() {
            return actual_package_size;
        }

        public void setActual_package_size(String actual_package_size) {
            this.actual_package_size = actual_package_size;
        }

        public String getPackage_type() {
            return package_type;
        }

        public void setPackage_type(String package_type) {
            this.package_type = package_type;
        }

        public String getTot_package_qty() {
            return tot_package_qty;
        }

        public void setTot_package_qty(String tot_package_qty) {
            this.tot_package_qty = tot_package_qty;
        }

        public PackageExtension getPackaging_extension() {
            return packaging_extension;
        }

        public void setPackaging_extension(PackageExtension packaging_extension) {
            this.packaging_extension = packaging_extension;
        }

        public PriceDetails getPrice_details() {
            return price_details;
        }

        public void setPrice_details(PriceDetails price_details) {
            this.price_details = price_details;
        }

        public KeyIdentifier getKey_identifier() {
            return key_identifier;
        }

        public void setKey_identifier(KeyIdentifier key_identifier) {
            this.key_identifier = key_identifier;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }


    public class PackageExtension {
        private String package_strength;
        private String strength_unit_of_measure;

        public String getPackage_strength() {
            return package_strength;
        }

        public void setPackage_strength(String package_strength) {
            this.package_strength = package_strength;
        }

        public String getStrength_unit_of_measure() {
            return strength_unit_of_measure;
        }

        public void setStrength_unit_of_measure(String strength_unit_of_measure) {
            this.strength_unit_of_measure = strength_unit_of_measure;
        }
    }

    public class PriceDetails {
        private String package_price;
        private String unit_price;
        private String price_type;

        public String getPackage_price() {
            return package_price;
        }

        public void setPackage_price(String package_price) {
            this.package_price = package_price;
        }

        public String getUnit_price() {
            return unit_price;
        }

        public void setUnit_price(String unit_price) {
            this.unit_price = unit_price;
        }

        public String getPrice_type() {
            return price_type;
        }

        public void setPrice_type(String price_type) {
            this.price_type = price_type;
        }
    }

    public class KeyIdentifier {
        private String formatted_id_number;
        private String storage_condition_code;

        public String getFormatted_id_number() {
            return formatted_id_number;
        }

        public void setFormatted_id_number(String formatted_id_number) {
            this.formatted_id_number = formatted_id_number;
        }

        public String getStorage_condition_code() {
            return storage_condition_code;
        }

        public void setStorage_condition_code(String storage_condition_code) {
            this.storage_condition_code = storage_condition_code;
        }
    }

}
