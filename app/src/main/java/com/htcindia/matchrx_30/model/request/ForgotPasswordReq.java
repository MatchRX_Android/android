package com.htcindia.matchrx_30.model.request;

/**
 * Created by sathishk on 11/21/2017.
 */

public class ForgotPasswordReq {

    private String username = "";
    private String dea = "";

    public ForgotPasswordReq(String username, String dea) {
        this.username = username;
        this.dea = dea;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDea() {
        return dea;
    }

    public void setDea(String dea) {
        this.dea = dea;
    }
}
