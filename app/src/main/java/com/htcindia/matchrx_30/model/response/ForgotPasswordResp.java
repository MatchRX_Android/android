package com.htcindia.matchrx_30.model.response;

import com.htcindia.matchrx_30.model.Status;

/**
 * Created by sathishk on 11/21/2017.
 */

public class ForgotPasswordResp extends Status{

    private ForgotPasswordData data;

    public ForgotPasswordData getData() {
        return data;
    }

    public void setData(ForgotPasswordData data) {
        this.data = data;
    }

    public class ForgotPasswordData{

        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
