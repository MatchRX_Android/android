package com.htcindia.matchrx_30.model;

/**
 * Created by sathishk on 11/22/2017.
 */

public class Status {

    private Integer code;
    private String status;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
