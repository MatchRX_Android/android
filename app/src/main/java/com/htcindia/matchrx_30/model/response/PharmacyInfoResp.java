package com.htcindia.matchrx_30.model.response;

import com.htcindia.matchrx_30.model.PharmacyInfo;
import com.htcindia.matchrx_30.model.Status;

/**
 * Created by sathishk on 11/22/2017.
 */

public class PharmacyInfoResp extends Status {

    private PharmacyInfo data;

    public PharmacyInfo getData() {
        return data;
    }

    public void setData(PharmacyInfo data) {
        this.data = data;
    }
}
