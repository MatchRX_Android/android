package com.htcindia.matchrx_30.model.response;

import com.htcindia.matchrx_30.model.Status;

/**
 * Created by sathishk on 11/24/2017.
 */

public class ErrorResponse extends Status {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
