package com.htcindia.matchrx_30.model.response;

import com.htcindia.matchrx_30.model.PharmacyInfo;
import com.htcindia.matchrx_30.model.Status;

/**
 * Created by sathishk on 11/21/2017.
 */

public class LoginResp extends Status {

    private LoginRespData data;

    public LoginRespData getData() {
        return data;
    }

    public void setData(LoginRespData data) {
        this.data = data;
    }

    public class LoginRespData extends PharmacyInfo{

        private String landing_page;
        private Integer notification_count;
        private Integer cart_count;
        private Integer sales_inprogress_count;
        private Integer purchase_inprogress_count;
        private String token_type;
        private Integer expires_in;
        private String access_token;
        private String refresh_token;
        private String message;

        public Integer getSales_inprogress_count() {
            return sales_inprogress_count;
        }

        public void setSales_inprogress_count(Integer sales_inprogress_count) {
            this.sales_inprogress_count = sales_inprogress_count;
        }

        public Integer getPurchase_inprogress_count() {
            return purchase_inprogress_count;
        }

        public void setPurchase_inprogress_count(Integer purchase_inprogress_count) {
            this.purchase_inprogress_count = purchase_inprogress_count;
        }

        public String getLanding_page() {
            return landing_page;
        }

        public void setLanding_page(String landing_page) {
            this.landing_page = landing_page;
        }

        public Integer getNotification_count() {
            return notification_count;
        }

        public void setNotification_count(Integer notification_count) {
            this.notification_count = notification_count;
        }

        public Integer getCart_count() {
            return cart_count;
        }

        public void setCart_count(Integer cart_count) {
            this.cart_count = cart_count;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public Integer getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(Integer expires_in) {
            this.expires_in = expires_in;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }



}
