package com.htcindia.matchrx_30.model.request;

/**
 * Created by sathishk on 11/21/2017.
 */

public class LoginReq {

    private String username = "";
    private String password = "";
    private String dea = "";

    public LoginReq(String dea, String username, String password) {
        this.username = username;
        this.password = password;
        this.dea = dea;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDea() {
        return dea;
    }

    public void setDea(String dea) {
        this.dea = dea;
    }
}

