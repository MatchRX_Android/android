package com.htcindia.matchrx_30;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.htcindia.matchrx_30.model.response.ForgotPasswordResp;
import com.htcindia.matchrx_30.utils.Internet;
import com.htcindia.matchrx_30.webservice.OnWebResponse;
import com.htcindia.matchrx_30.webservice.WebRequest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgotPasswordActivity extends BaseActivity implements OnWebResponse {

    EditText etDeaNo;
    EditText etUsername;
    View forgotPasswordView;
    TextView btnReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_forgot_password);

        init();

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    resetPasswordWebServiceCall();
                }
            }
        });
    }

    public void resetPasswordWebServiceCall() {

        if (Internet.isConnected(this, true, forgotPasswordView)) {

            showLoadingView();

            String deaNo = etDeaNo.getText().toString();
            String username = etUsername.getText().toString();

            new WebRequest(ForgotPasswordActivity.this, ForgotPasswordActivity.this, WebRequest.Type.FORGOT_PASSWORD).ForgotPasswordRequest(deaNo, username);
        }
    }

    private void init() {

        etDeaNo = findViewById(R.id.etDeaNo);
        etUsername = findViewById(R.id.etUsername);
        forgotPasswordView = findViewById(R.id.rlForgotPassword);
        btnReset = findViewById(R.id.btnReset);
    }

    private boolean validate() {

        Pattern p;
        Matcher m;

        //DEA Number VALIDATIONS
        p = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE);
        m = p.matcher(etDeaNo.getText());

        if (etDeaNo.getText().toString().length() == 0 || m.find() || (etDeaNo.getText().toString().length() < 9)
                || !Character.isLetter(etDeaNo.getText().charAt(0)) || Character.isLetter(etDeaNo.getText().charAt(2))) {
            showSnackBar(forgotPasswordView, getString(R.string.dea_error_message));
            return false;
        }

        String subStr = etDeaNo.getText().toString().substring(2);
        try {
            Integer.parseInt(subStr);
        } catch (Exception e) {
            showSnackBar(forgotPasswordView, getString(R.string.dea_error_message));
            return false;
        }

        //Username Validations
        p = Pattern.compile("[^a-z0-9._-]");
        m = p.matcher(etUsername.getText());

        if (m.find() || etUsername.getText().toString().length() == 0 || Character.isDigit(etUsername.getText().toString().charAt(0))
                || etUsername.getText().toString().length() < 5 || (etUsername.getText().toString().charAt(0) == '-') || (etUsername.getText().toString().charAt(0) == '.')
                || etUsername.getText().toString().charAt(0) == '_') {

            showSnackBar(forgotPasswordView, getString(R.string.username_error_message));
            return false;
        }

//        if (etDeaNo.getText().toString().length() > 0) {
//            if ((etDeaNo.getText().toString().length() < 9) || !Character.isLetter(etDeaNo.getText().charAt(0)) || Character.isLetter(etDeaNo.getText().charAt(2))) {
//                showSnackBar(forgotPasswordView, getString(R.string.dea_error_message));
//                return false;
//            }
//
//            String subStr = etDeaNo.getText().toString().substring(2);
//            try {
//                Integer.parseInt(subStr);
//            } catch (Exception e) {
//                showSnackBar(forgotPasswordView, getString(R.string.dea_error_message));
//                return false;
//            }
//
//        } else {
//            showSnackBar(forgotPasswordView, getString(R.string.dea_error_message));
//            return false;
//        }
//
//        if (etUsername.getText().toString().length() == 0 || Character.isDigit(etUsername.getText().toString().charAt(0)) || etUsername.getText().toString().length() < 5) {
//            showSnackBar(forgotPasswordView, getString(R.string.username_error_message));
//            return false;
//        }

        return true;
    }

    @Override
    public void onSuccess(Object response, int type) {
        ForgotPasswordResp forgotPasswordResp = (ForgotPasswordResp) response;
        if (forgotPasswordResp.getStatus().equals("OK")) {
            showResetDialog(forgotPasswordResp.getData().getMessage(), ForgotPasswordActivity.this);
        } else {
            showSnackBar(forgotPasswordView, forgotPasswordResp.getData().getMessage());
        }
        hideLoadingView();

    }

    @Override
    public void onFailure(String reason, int type) {
        hideLoadingView();
        showSnackBar(forgotPasswordView, reason);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
        finish();
    }

    protected void showResetDialog(String message, Context context) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
                finish();
            }
        }).show();
    }
}
