package com.htcindia.matchrx_30;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.htcindia.matchrx_30.utils.MatchRx;
import com.htcindia.matchrx_30.utils.Preferences;
import com.htcindia.matchrx_30.utils.ScreenSize;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_splash);

        getScreenSize();

        MatchRx.PREF = new Preferences(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, 3000);
    }


    private void getScreenSize() {
        WindowManager localWindowManager = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);
        localWindowManager.getDefaultDisplay().getMetrics(new DisplayMetrics());
        Point localPoint = getSize(localWindowManager);
        ScreenSize.Width = localPoint.x;
        ScreenSize.Height = localPoint.y;
    }

    @SuppressLint("NewApi")
    private Point getSize(WindowManager paramWindowManager) {
        Point localPoint = new Point();
        if (Build.VERSION.SDK_INT >= 13) {
            paramWindowManager.getDefaultDisplay().getSize(localPoint);
            return localPoint;
        }
        Display localDisplay = paramWindowManager.getDefaultDisplay();
        localPoint.x = localDisplay.getWidth();
        localPoint.y = localDisplay.getHeight();
        return localPoint;
    }
}
