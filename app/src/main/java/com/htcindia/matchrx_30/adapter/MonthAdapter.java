package com.htcindia.matchrx_30.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by sathishk on 12/11/2017.
 */

public class MonthAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private int month;
    private boolean isCurrentYear;
    private ArrayList<String> months = new ArrayList<String>();


    public MonthAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        addMonths();
        setCurrentMonth();
    }


    @Override
    public int getCount() {
        return months.size();
    }

    @Override
    public String getItem(int position) {
        return months.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (view == null) {
            view = inflater.inflate(R.layout.calendar_item_view, null);
        }

        TextView calendarTxt = view.findViewById(R.id.calendarTxt);
        calendarTxt.setText(getItem(position));

        if (isCurrentYear) {
            if (position < month) {
                calendarTxt.setTextColor(ContextCompat.getColor(mContext, R.color.sell_post_item_tab_unhighlight_no));
            }
        }

        return view;
    }

    private void setCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        month = calendar.get(Calendar.MONTH);
    }

    public void isCurrentYear(boolean isCurrentYear) {
        this.isCurrentYear = isCurrentYear;
    }

    private void addMonths() {
        months.add("JAN");
        months.add("FEB");
        months.add("MAR");
        months.add("APR");
        months.add("MAY");
        months.add("JUN");
        months.add("JUL");
        months.add("AUG");
        months.add("SEP");
        months.add("OCT");
        months.add("NOV");
        months.add("DEC");
    }
}
