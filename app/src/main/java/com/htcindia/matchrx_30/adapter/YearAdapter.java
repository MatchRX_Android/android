package com.htcindia.matchrx_30.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;

import java.util.Calendar;

/**
 * Created by sathishk on 12/11/2017.
 */

public class YearAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private int currentYear;
    private int currentMonth;

    public YearAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setCurrentYear();
    }

    @Override
    public int getCount() {
        return 21;
    }

    @Override
    public String getItem(int position) {
        int tempYear;
        if (currentMonth < 11) {
            tempYear = position + currentYear;
        } else {
            position++;
            tempYear = position + currentYear;
        }
        return String.valueOf(tempYear);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (view == null) {
            view = inflater.inflate(R.layout.calendar_item_view, null);
        }
        ((TextView) view.findViewById(R.id.calendarTxt)).setText(getItem(position));
        return view;
    }

    private void setCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        currentYear = calendar.get(Calendar.YEAR);
        currentMonth = calendar.get(Calendar.MONTH);
    }
}
