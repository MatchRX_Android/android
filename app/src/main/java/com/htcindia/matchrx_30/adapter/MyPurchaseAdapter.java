package com.htcindia.matchrx_30.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;

/**
 * Created by sathishk on 11/29/2017.
 */

public class MyPurchaseAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;

    public MyPurchaseAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflater.inflate(R.layout.my_purchase_list_item, null);
        if (position == 0) {
            view.findViewById(R.id.textView).setBackgroundColor(Color.parseColor("#ff1d1d"));
            ((TextView) view.findViewById(R.id.textView)).setTextColor(Color.WHITE);
        }
        return view;
    }

}
