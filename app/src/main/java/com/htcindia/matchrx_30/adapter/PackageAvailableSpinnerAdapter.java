package com.htcindia.matchrx_30.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;

/**
 * Created by sathishk on 12/11/2017.
 */

public class PackageAvailableSpinnerAdapter extends BaseAdapter {

    private int mCount = 0;
    private Context mContext;
    private LayoutInflater inflater;

    public PackageAvailableSpinnerAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setPackageAvailableCount(int mCount) {
        this.mCount = mCount;
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public String getItem(int position) {
        int num = position + 1;
        return "" + num;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.package_available_apinner_item, null);
        ((TextView) view.findViewById(R.id.tvCount)).setText(getItem(position));
        return view;
    }
}
