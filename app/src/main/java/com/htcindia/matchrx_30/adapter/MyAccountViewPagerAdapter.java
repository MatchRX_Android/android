package com.htcindia.matchrx_30.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;
import com.htcindia.matchrx_30.fragment.myaccount.LicenseFragment;
import com.htcindia.matchrx_30.fragment.myaccount.OwnerFragment;
import com.htcindia.matchrx_30.fragment.myaccount.PharmacyFragment;
import com.htcindia.matchrx_30.model.PharmacyInfo;

import java.security.acl.Owner;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sathishk on 11/22/2017.
 */

public class MyAccountViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<String> mFragmentTitleList = new ArrayList<>();
    private PharmacyInfo pharmacyInfo;

    public MyAccountViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

}
