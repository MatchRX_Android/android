package com.htcindia.matchrx_30.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.htcindia.matchrx_30.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sathishk on 11/17/2017.
 */

public class HamburgerMenuAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private List<String> menuNames;
    private List<Integer> menuIcons;

    public HamburgerMenuAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        menuNames = Arrays.asList(mContext.getString(R.string.buy),
                mContext.getString(R.string.sell),
                mContext.getString(R.string.my_orders),
                mContext.getString(R.string.dashboard),
                mContext.getString(R.string.wishlist),
                mContext.getString(R.string.my_account),
                mContext.getString(R.string.settings));
        menuIcons = Arrays.asList(R.drawable.ic_buy, R.drawable.ic_sell, R.drawable.ic_my_orders, R.drawable.ic_dashboard, R.drawable.ic_wishlist, R.drawable.ic_my_account, R.drawable.ic_settings);
    }

    @Override
    public int getCount() {
        return menuNames.size();
    }

    @Override
    public String getItem(int position) {
        return menuNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.hamburger_menu_item_view, null);

        TextView tvMenuName = view.findViewById(R.id.tvMenu);
        ImageView ivMenuIcon = view.findViewById(R.id.ivMenuIcon);

        tvMenuName.setText(getItem(position));
        ivMenuIcon.setImageDrawable((mContext.getResources().getDrawable(menuIcons.get(position), mContext.getTheme())));

        return view;
    }
}
