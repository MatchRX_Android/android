package com.htcindia.matchrx_30;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TabWidget;
import android.widget.TextView;

import com.htcindia.matchrx_30.model.Token;
import com.htcindia.matchrx_30.model.response.LoginResp;
import com.htcindia.matchrx_30.utils.Constants;
import com.htcindia.matchrx_30.utils.Internet;
import com.htcindia.matchrx_30.utils.MatchRx;
import com.htcindia.matchrx_30.webservice.OnWebResponse;
import com.htcindia.matchrx_30.webservice.WebRequest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends BaseActivity implements View.OnClickListener, OnWebResponse {

    View loginView;
    EditText etDeaNo;
    EditText etUsername;
    EditText etPassword;
    TextView tvForgotPassword;
    Button btnLogin;
    CheckBox cbRememberMe;
    SharedPreferences sharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_login);
        init();
        setRememberedData();
        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);

    }

    private void onLoginClick() {
        if (validate()) {
            loginWebServiceCall();
        }
    }

    public void loginWebServiceCall() {

        if (Internet.isConnected(this, true, loginView)) {

            showLoadingView();

            String deaNo = etDeaNo.getText().toString();
            String username = etUsername.getText().toString();
            String password = etPassword.getText().toString();

            new WebRequest(this, this, WebRequest.Type.LOGIN).LoginRequest(deaNo, username, password);

        }
    }

    private void onForgotPasswordClick() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
        finish();
    }

    private boolean validate() {

        Pattern p;
        Matcher m;

        //DEA Number VALIDATIONS
        p = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE);
        m = p.matcher(etDeaNo.getText());

        if (etDeaNo.getText().toString().length() == 0 || m.find() || (etDeaNo.getText().toString().length() < 9)
                || !Character.isLetter(etDeaNo.getText().charAt(0)) || Character.isLetter(etDeaNo.getText().charAt(2))) {
            showSnackBar(loginView, getString(R.string.dea_error_message));
            return false;
        }

        String subStr = etDeaNo.getText().toString().substring(2);
        try {
            Integer.parseInt(subStr);
        } catch (Exception e) {
            showSnackBar(loginView, getString(R.string.dea_error_message));
            return false;
        }

        //Username Validations
        p = Pattern.compile("[^a-z0-9._-]");
        m = p.matcher(etUsername.getText());

        if (m.find() || etUsername.getText().toString().length() == 0 || Character.isDigit(etUsername.getText().toString().charAt(0))
                || etUsername.getText().toString().length() < 5 || (etUsername.getText().toString().charAt(0) == '-') || (etUsername.getText().toString().charAt(0) == '.')
                || etUsername.getText().toString().charAt(0) == '_') {

            showSnackBar(loginView, getString(R.string.username_error_message));
            return false;
        }

        if (etPassword.getText().toString().trim().length() == 0) {
            showSnackBar(loginView, getString(R.string.password_error_message));
            return false;
        }

        return true;
    }

    private void init() {

        loginView = findViewById(R.id.rlLogin);
        etDeaNo = findViewById(R.id.etDeaNo);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        btnLogin = findViewById(R.id.btnLogin);

        cbRememberMe = findViewById(R.id.cbRememberMe);

        sharedPrefs = this.getPreferences(MODE_PRIVATE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvForgotPassword:
                onForgotPasswordClick();
                break;

            case R.id.btnLogin:
                onLoginClick();
                break;
        }
    }

    @Override
    public void onSuccess(Object response, int type) {
        LoginResp loginResp = (LoginResp) response;

        if (loginResp.getStatus().equals("OK")) {

            //Saving DEA# and Username in shared preferences
            saveRememberData();

            //Setting token details globally
            Token token = new Token();
            token.setAccess_token(loginResp.getData().getAccess_token());
            token.setRefresh_token(loginResp.getData().getRefresh_token());
            token.setExpires_in(loginResp.getData().getExpires_in());
            token.setToken_type(loginResp.getData().getToken_type());
            MatchRx.TOKEN = token;

            Bundle bundle = new Bundle();
            bundle.putString(getString(R.string.dea_no), loginResp.getData().getPharmacy().getDea());
            bundle.putString(getString(R.string.owner_name), loginResp.getData().getFirst_name() + " " + loginResp.getData().getLast_name());
            bundle.putString(getString(R.string.pharmacy_name), loginResp.getData().getPharmacy().getLegal_business_name());
            bundle.putLong(getString(R.string.pharmacy_id), loginResp.getData().getPharmacy_id());
            bundle.putString(getString(R.string.landing_page), loginResp.getData().getLanding_page());

            String deaNo = loginResp.getData().getPharmacy().getDea();

            Constants.STATE_WISE_RESTRICTION = "Bothallowed";

            if (deaNo.equalsIgnoreCase("BG4827765")) {
                Constants.STATE_WISE_RESTRICTION = "OnlySealed";
            } else if (deaNo.equalsIgnoreCase("BC7924322")) {
                Constants.STATE_WISE_RESTRICTION = "OnlyNon-sealed";
            } else if (deaNo.equalsIgnoreCase("BO9471830")) {
                Constants.STATE_WISE_RESTRICTION = "Bothallowed";
            }

            Intent intent = new Intent(this, LandingActivity.class);
            intent.putExtra(getString(R.string.pharmacy_info), bundle);
            startActivity(intent);
            finish();

        } else {
            showSnackBar(loginView, loginResp.getData().getMessage());
        }
        hideLoadingView();

    }

    @Override
    public void onFailure(String reason, int type) {
        hideLoadingView();
        showSnackBar(loginView, reason);
    }

    private void setRememberedData() {
        String savedDeaNo = "";
        if (sharedPrefs.contains(getString(R.string.sp_dea_no))) {
            savedDeaNo = sharedPrefs.getString(getString(R.string.sp_dea_no), "");
            etDeaNo.setText(savedDeaNo);
        }

        String savedUsername = "";
        if (sharedPrefs.contains(getString(R.string.sp_username))) {
            savedUsername = sharedPrefs.getString(getString(R.string.sp_username), "");
            etUsername.setText(savedUsername);
        }

//        if (sharedPrefs.contains(getString(R.string.sp_is_remember_checked))) {
//            boolean isRememberChecked = sharedPrefs.getBoolean(getString(R.string.sp_is_remember_checked), false);
//            cbRememberMe.setChecked(isRememberChecked);
//        }

        if (savedDeaNo.length() == 0 && savedUsername.length() == 0) {
            cbRememberMe.setChecked(false);
        }
    }

    private void saveRememberData() {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        if (cbRememberMe.isChecked()) {
            editor.putString(getString(R.string.sp_dea_no), etDeaNo.getText().toString());
            editor.putString(getString(R.string.sp_username), etUsername.getText().toString());
//            editor.putBoolean(getString(R.string.sp_is_remember_checked), cbRememberMe.isChecked());
            editor.commit();
        } else {
            if (sharedPrefs.getString(getString(R.string.sp_dea_no), "").equals(etDeaNo.getText().toString())) {
                editor.remove(getString(R.string.sp_dea_no));
            }

            if (sharedPrefs.getString(getString(R.string.sp_username), "").equals(etUsername.getText().toString())) {
                editor.remove(getString(R.string.sp_username));
            }
//            editor.putBoolean(getString(R.string.sp_is_remember_checked), cbRememberMe.isChecked());
            editor.commit();
        }
    }
}
