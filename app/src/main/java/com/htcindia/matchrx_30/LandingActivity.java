package com.htcindia.matchrx_30;

import android.*;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.htcindia.matchrx_30.adapter.HamburgerMenuAdapter;
import com.htcindia.matchrx_30.fragment.buy.BuyFragment;
import com.htcindia.matchrx_30.fragment.dashboard.DashboardFragment;
import com.htcindia.matchrx_30.fragment.myaccount.PharmacyInfoFragment;
import com.htcindia.matchrx_30.fragment.myorders.MyOrdersFragment;
import com.htcindia.matchrx_30.fragment.sell.SellFragment;
import com.htcindia.matchrx_30.fragment.settings.SettingsFragment;
import com.htcindia.matchrx_30.fragment.wishlist.WishlistFragment;
import com.htcindia.matchrx_30.listeners.ScannerListener;
import com.htcindia.matchrx_30.listeners.SnackBarListener;
import com.htcindia.matchrx_30.utils.Constants;
import com.htcindia.matchrx_30.utils.MatchRx;
import com.theartofdev.edmodo.cropper.CropImage;

public class LandingActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, ScannerListener, SnackBarListener {

    DrawerLayout drawerLayout;
    NavigationView navigationMenu;
    ImageView ivDrawer;
    ListView lvMenuItems;

    TextView tvTitle;
    View llLogout;

    FrameLayout landing_container;

    HamburgerMenuAdapter hamburgerMenuAdapter;

    PharmacyInfoFragment pharmacyInfoFragment;
    BuyFragment buyFragment;
    SellFragment sellFragment;
    MyOrdersFragment myOrdersFragment;
    DashboardFragment dashboardFragment;
    WishlistFragment wishlistFragment;
    SettingsFragment settingsFragment;

    long pharmacyId;
    String landingPage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_landing);

        init();

        //Hamburger Menu Setup
        if (getIntent().hasExtra(getString(R.string.pharmacy_info))) {
            Bundle bundle = getIntent().getBundleExtra(getString(R.string.pharmacy_info));

            String ownerName = bundle.getString(getString(R.string.owner_name));
            String pharmacyName = bundle.getString(getString(R.string.pharmacy_name));
            String deaNo = bundle.getString(getString(R.string.dea_no));
            pharmacyId = bundle.getLong(getString(R.string.pharmacy_id));
            landingPage = bundle.getString(getString(R.string.landing_page));

            ((TextView) findViewById(R.id.tvOwnerName)).setText("Hi, " + ownerName);
            ((TextView) findViewById(R.id.tvPharmacyName)).setText(pharmacyName);
            ((TextView) findViewById(R.id.tvDeaNo)).setText("DEA#  " + deaNo);
        }

        lvMenuItems.setAdapter(hamburgerMenuAdapter);
        lvMenuItems.setOnItemClickListener(this);

        llLogout.setOnClickListener(this);

        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(navigationMenu);
            }
        });

        //Set the landing fragment - MyOrders or Buy
        myOrdersFragment.setLandingPageType(landingPage);
        if (landingPage.equals(Constants.SELLER_CONFIRMATION) || landingPage.equals(Constants.BUYER_CONFIRMATION)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, myOrdersFragment).commit();
        } else if (landingPage.equals(Constants.MARKETPLACE)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, buyFragment).commit();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        drawerLayout.closeDrawer(navigationMenu);

        switch (position) {

            case 0:
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, buyFragment).commit();
                break;

            case 1:
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, getSellFragment()).commit();
                break;

            case 2:
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, myOrdersFragment).commit();
                break;

            case 3:
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, dashboardFragment).commit();
                break;

            case 4:
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, wishlistFragment).commit();
                break;

            case 5:
                setUpPharmacyInfoFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, pharmacyInfoFragment).commit();
                break;

            case 6:
                getSupportFragmentManager().beginTransaction().replace(R.id.landing_container, settingsFragment).commit();
                break;
        }
    }

    private SellFragment getSellFragment() {
        sellFragment = new SellFragment();
        sellFragment.setScannerListener(this);
        sellFragment.setSnackBarListener(this);
        return sellFragment;
    }

    private void setUpPharmacyInfoFragment() {
        if (pharmacyInfoFragment == null) {
            Bundle bundle = new Bundle();
            bundle.putLong(getString(R.string.pharmacy_id), pharmacyId);
            pharmacyInfoFragment = new PharmacyInfoFragment();
            pharmacyInfoFragment.setView(drawerLayout);
            pharmacyInfoFragment.setArguments(bundle);
        }
    }

    private void init() {
        ivDrawer = findViewById(R.id.ivDrawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationMenu = findViewById(R.id.navigation);
        lvMenuItems = findViewById(R.id.lvMenuItems);
        landing_container = findViewById(R.id.landing_container);

        tvTitle = findViewById(R.id.tvTitle);
        llLogout = findViewById(R.id.llLogout);

        hamburgerMenuAdapter = new HamburgerMenuAdapter(this);

        buyFragment = new BuyFragment();
        myOrdersFragment = new MyOrdersFragment();
        dashboardFragment = new DashboardFragment();
        wishlistFragment = new WishlistFragment();
        settingsFragment = new SettingsFragment();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationMenu)) {
            drawerLayout.closeDrawer(navigationMenu);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void setTitle(CharSequence title) {
        tvTitle.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llLogout:
                drawerLayout.closeDrawer(navigationMenu);
                showLogoutAlert(getString(R.string.logout_alert_messages), this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.SCANNING_REQ_CODE && resultCode == RESULT_OK) {
            if (data.hasExtra("NDC_BARCODE_NO")) {
                sellFragment.searchNDCDrug(data.getStringExtra("NDC_BARCODE_NO"));
            }
        }
    }

    @Override
    public void startScanner() {

        Intent scannerIntent = new Intent(LandingActivity.this, ScanningActivity.class);

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        Constants.PERMISSION_REQUEST_CAMERA);
            } else {
                startActivityForResult(scannerIntent, Constants.SCANNING_REQ_CODE);
            }
        } else {
            startActivityForResult(scannerIntent, Constants.SCANNING_REQ_CODE);
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(new Intent(LandingActivity.this, ScanningActivity.class), Constants.SCANNING_REQ_CODE);
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                            1003);
                }
                break;
        }
    }

    @Override
    public void showSnackBar(String msg) {
        showSnackBar(drawerLayout, msg);
    }
}
